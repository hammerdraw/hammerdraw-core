import re

from hammerdraw.modules.keystone import KeystoneCardCompiler
from hammerdraw.util.text_to_raw import parse_text_file
from scripts.common import setup

def main():
    logger = setup()
    
    separator = '============================='
    
    regex = re.compile(rf'''(?P<name>.+)
(?P<rarity>.+)
(?P<race>\w+) (?P<type>.+)
(?P<minerals>\d+) *[/ ] *(?P<gas>\d+)

(?P<effect>(.|\s)*)''')
    
    common = dict \
    (
        set="hammerdraw/king-cobra",
        image="images/{filename}.png",
    )
    
    code = 0
    # file = '''D:\Keystone\Kel-Morian.txt'''
    # file = '''D:\Keystone\Terran Mech.txt'''
    # file = '''D:\Keystone\Random.txt'''
    # file = '''D:\Keystone\Lobster.txt'''
    # file = '''D:\Keystone\Trash Contest Winners.txt'''
    file = '''D:\Keystone\In Favour of KingCobra.txt'''
    with (open(file)) as _data:
        code = parse_text_file \
        (
            compiler = KeystoneCardCompiler,
            file = _data,
            add_fields = common,
            regex = regex,
            logger = logger,
            multiline_fields = [ 'effect' ],
            expand_fields = [ 'image' ],
            decapitalize_fields = [ 'type', 'rarity', 'race' ],
            numeric_fields = [ 'minerals', 'gas' ],
        )
    
    return code

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
