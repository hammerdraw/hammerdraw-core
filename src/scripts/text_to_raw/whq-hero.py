import re

from hammerdraw.modules.warhammer_quest import HeroCompiler
from hammerdraw.util.text_to_raw import parse_text_file

def main():
    regex = re.compile(r'''(?P<name>.+?)( +\((?P<subtitle>.+)\))?
(?P<stats>(?P<stats_move>\d+) *[/ ] *(?P<stats_agility>\d+)\+? *[/ ] *(?P<stats_save>\d+)\+?)
(?P<traits>(?:(?P<trait>\w+),? *)+)
(?i:renown):(?P<renown>(.|\s)*?)

(?P<weapons>(?P<weapon>^(?P<weapon_name>[^:\n]+?) +\((?P<weapon_cost>\d+)\+?\) +(?P<weapon_range>\w+) +(?P<weapon_hit>\d+)\+ +(?P<weapon_damage>[^\n]+)(?:\n|$))+)

(?P<abilities>(?P<ability>^(?P<ability_name>[^:\n]+?)(?: *\((?P<ability_cost>\d+)\+?(?P<ability_diceSpace>, +dice)?\))?: *(?P<ability_description>(?:.+(?:\n|$))*)(?:\n|$))*)''', re.MULTILINE)
    
    # file = '''D:\Hammerhal\Heroes\All Heroes.txt'''
    file = '''D:\Hammerhal\Heroes\Custom Heroes.txt'''
    with (open(file)) as _data:
        code = parse_text_file \
        (
            compiler = HeroCompiler,
            file = _data,
            regex = regex,
            multiline_fields = [ 'renown', 'abilities.[ability].description' ],
            expand_fields = [ 'image' ],
            decapitalize_fields = [ 'traits.[trait]', 'weapons.[weapon].range' ],
            numeric_fields = [ 'stats.move', 'stats.agility', 'stats.save', 'weapons.[weapon].cost', 'weapons.[weapon].hit', 'abilities.[ability].cost' ],
            list_fields = dict(abilities='ability', traits='trait', weapons='weapon'),
            structure_fields =
            {
                'abilities.[ability]': ('ability', [ 'name', 'cost', 'description', 'diceSpace' ]),
                'weapons.[weapon]': ('weapon', [ 'name', 'cost', 'range', 'hit', 'damage' ]),
                'stats': ('stats', [ 'move', 'agility', 'save' ]),
            },
            add_fields =
            {
                'image': 'images/{filename}.jpg',
            },
            update_fields =
            {
                'abilities.[ability].diceSpace': lambda s: bool(s),
            },
            separator = '=============================',
        )
    
    return code

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
