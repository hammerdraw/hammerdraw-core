import json
import os
import shutil
import sys
import tempfile
import urllib.parse
import urllib.request
from http.client import HTTPResponse
from typing import *

GROUP_NAME = 'hammerdraw'
ROOT_URL = f'https://gitlab.com/{GROUP_NAME}'
GITLAB_API_ROOT = 'https://gitlab.com/api/v4'
LINKS = \
[
    ('module', 'modules/{}', '../module-{}'),
    ('data',   'raw/{}',     '../module-{}-data'),
]

def create_directories():
    for _, dir, _ in LINKS:
        d = os.path.dirname(dir)
        if (not os.path.isdir(d)):
            os.mkdir(d, 0o755)

def download_module(module_name: str, module_system_name, *, branch: str = None):
    for repo_type, _, dir in LINKS:
        dir = dir.format(module_system_name)
        
        if (not os.path.isdir(dir.format(module_system_name))):
            _branch = branch
            if (_branch is None):
                encoded_project_name = f'{GROUP_NAME}/{module_name}/{repo_type}'.replace('/', '%2f')
                url = f'{GITLAB_API_ROOT}/projects/{encoded_project_name}'
                print(f"Requesting url: {url!r} ...")
                resp: HTTPResponse = urllib.request.urlopen(url)
                resp_json: Dict[str, Any] = json.load(resp)
                _branch: str = resp_json['default_branch']
            
            url = f'{ROOT_URL}/{module_name}/{repo_type}/-/archive/{_branch}/{repo_type}-{_branch}.tar.gz'
            print(f"Downloading archive {url!r} ...")
            
            temp_dir = os.path.abspath(tempfile.mkdtemp())
            archive_name: str = os.path.basename(url)
            archive_path = os.path.join(temp_dir, archive_name)
            urllib.request.urlretrieve(url, archive_path)
            
            print(f"Unpacking archive...")
            shutil.unpack_archive(archive_path, temp_dir)
            extracted_name = archive_name.rpartition('.')[0].rpartition('.')[0]
            shutil.move(os.path.join(temp_dir, extracted_name), dir)
            
            print("Cleanup...")
            os.remove(archive_path)
            os.rmdir(temp_dir)

def make_link(target, link_name):
    target = os.path.relpath(target, os.path.dirname(link_name))
    
    if (os.path.islink(link_name)):
        if (os.readlink(link_name) == target):
            return
        else:
            print(f"Symlink {link_name} is broken / points the wrong way, recreating it...")
            os.unlink(link_name)
    
    print(f"Making symlink: {link_name!r} ==> {target!r} ...")
    os.symlink(target, link_name, True)

def create_links(module_system_name: str):
    make_link('modules', 'src/modules')
    
    for _, link_name, target in LINKS:
        link_name = link_name.format(module_system_name)
        target = target.format(module_system_name)
        
        make_link(target, link_name)


def main():
    args = sys.argv[1:]
    if (not args):
        print("Missing argument: module name")
        return 1
    
    for module_name in args:
        module_system_name = module_name.replace('-', '_')
        
        print(f"Installing module {module_name}...")
        create_directories()
        download_module(module_name, module_system_name)
        create_links(module_system_name)
        
        print(f"Module {module_name} successfully installed")
    return 0

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
