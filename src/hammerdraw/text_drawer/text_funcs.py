from typing import *

def capitalize_first(s: str) -> str:
    """
    Converts the first character in the string to the uppercase.
    Unlike s.capitalize(), this function is not aggressive.
    
    Examples:
     - capitalize_first("hello world") == "Hello world"
     - capitalize_first("hello, World!") == "Hello, World!"
     - capitalize_first("UK") == "UK"
    
    :param s: str
    :return: str
    """
    
    if (len(s) > 0):
        return s[0].upper() + s[1:]
    return s

def decapitalize_first(s: str) -> str:
    """
    Converts the first character in the string to the lowercase.
    
    Examples:
     - capitalize_first("Hello world") == "hello world"
     - capitalize_first("Hello, World!") == "hello, World!"
     - capitalize_first("UK") == "uK"
    
    :param s: str
    :return: str
    """
    
    if (len(s) > 0):
        return s[0].lower() + s[1:]
    return s

capitalize = str.title
"""
Alias for s.title() which is kept for backwards-compatibility
"""

decapitalize = str.lower
"""
Alias for s.lower() which is kept for backwards-compatibility
"""

def capitalize_smart(s: str) -> str:
    """
    Converts the first character in each word in the string to the uppercase.
    Unlike s.title(), this function is not aggressive.
    
    Examples:
     - capitalize_first("hello world") == "Hello World"
     - capitalize_first("hello, World!") == "Hello, World!"
     - capitalize_first("UK") == "UK"
    
    :param s: str
    :return: str
    """
    
    return ' '.join(capitalize_first(_word) for _word in s.split(' '))

def decapitalize_smart(s: str) -> str:
    """
    Converts the first character in each word in the string to the lowercase.
    
    Examples:
     - capitalize_first("Hello world") == "hello world"
     - capitalize_first("Hello, World!") == "hello, world!"
     - capitalize_first("UK") == "uK"
    
    :param s: str
    :return: str
    """
    
    return ' '.join(decapitalize_first(_word) for _word in s.split(' '))

T = TypeVar('T')
def iter_index(seq: Sequence[T], element: T, start: int = None, end: int = None, *, method: Union[Callable[[Sequence[T], Any, int, int], int], str] = 'index') -> Iterator[int]:
    """
    Searches the sequence `seq`, finding the element `element`, using the method `method` (default: `'index'`),
    starting from element #`start` (default: 0), until the element #`end` (default: sequence's end).
    Returns an iterator of indices of matching elements.
    
    :param seq: Sequence[T]
    :param element: T
    :param start: Optional[int]
    :param end: Optional[int]
    :param method: str -- method name, or function: (Sequence[T], T, int, int) => int
    :return: Iterator[int]
    """
    
    if (isinstance(method, str)):
        method = getattr(type(seq), method)
    if (start is None):
        start = 0
    if (end is None):
        end = len(seq)
    
    while True:
        try:
            i = method(seq, element, start, end)
        except ValueError:
            break
        else:
            yield i
            start = i + 1

def index_of_any(seq: Sequence[T], elements: Iterable[T], start: int = None, end: int = None) -> int:
    """
    Searches the sequence `seq`, finding the first entry of any element from `elements`,
    starting from element #`start` (default: 0), until the element #`end` (default: sequence's end).
    Returns the index of the first matching element.
    
    Raises StopIteration if element is not presented in the sequence
    
    :param seq: Sequence[T]
    :param elements: Iterable[T]
    :param start: Optional[int]
    :param end: Optional[int]
    :return: int
    """
    
    if (start is None):
        start = 0
    if (end is None):
        end = len(seq)
    
    _find = set(elements) 
    for i in range(start, end):
        x = seq[i]
        if (x in _find):
            return i
    
    raise StopIteration("Elements not presented in sequence")
del T

__all__ = \
[
    'capitalize',
    'capitalize_first',
    'capitalize_smart',
    'decapitalize',
    'decapitalize_first',
    'decapitalize_smart',
    'index_of_any',
    'iter_index',
]
