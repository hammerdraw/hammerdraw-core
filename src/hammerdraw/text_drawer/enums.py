import inspect
from enum import Enum, auto
from typing import *

from dataclasses_config import deserialize_with, ConstructorDataType

@deserialize_with(ConstructorDataType.String)
class ExtendedEnum(Enum):
    @classmethod
    def find_value(cls, key) -> Optional['ExtendedEnum']:
        if (isinstance(key, str)):
            _members = inspect.getmembers(cls)
            for _name, _value in _members:
                if (_name == key):
                    # print(f"{_name!r} is {_value!r}!")
                    return _value
            
            # print(f"{key!r} not found")
            return None
        
        else:
            # print(f"{key!r} is not a string; returning it")
            return key
    
    @classmethod
    def _missing_(cls, value):
        if (value is None):
            return None
        else:
            result = cls.find_value(key=value)
            if (result is not None):
                return result
            else:
                return super()._missing_(value)

class CapitalizationModes(ExtendedEnum):
    Normal = auto()
    UpperCase = auto()
    AllCaps = auto()
    LowerCase = auto()
    NoCaps = auto()
    Capitalize = auto()
    CapitalizeFirst = auto()
    CapitalizeSmart = auto()
    SmallCaps = auto()

class TextAlignment(ExtendedEnum):
    Left = auto()
    Top = auto()
    Center = auto()
    Right = auto()
    Bottom = auto()
    Justify = auto()

class PrintModes(ExtendedEnum):
    NormalPrint = auto()
    GetTextSize = auto()
    SplitLines = auto()

__all__ = \
[
    'CapitalizationModes',
    'PrintModes',
    'TextAlignment',
]
