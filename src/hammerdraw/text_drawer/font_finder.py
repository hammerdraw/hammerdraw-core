import glob
import os
from logging import getLogger
from typing import *

from fontTools.ttLib import TTFont

from dataclasses_config import Path

logger = getLogger('text_drawer.fond_finder')

"""
From:
 - https://github.com/gddc/ttfquery/blob/master/ttfquery/describe.py
 - http://www.starrhorne.com/2012/01/18/how-to-extract-font-names-from-ttf-files-using-python-and-our-old-friend-the-command-line.html
 - https://gist.github.com/pklaus/dce37521579513c574d0
ported to Python 3

Heavily modified for font-finding & cross-platform by USSX.Hares / Peter Zaitcev
"""

FontFamilyName = NewType('FontFamilyName', str)
FontName = NewType('FontName', str)

class Record:
    string: bytes
    nameID: int

class FontFinder:
    
    _FONT_SPECIFIER_NAME_ID = 4
    _FONT_SPECIFIER_FAMILY_ID = 1
    
    def __init__(self):
        self.cached: Dict[FontFamilyName, Dict[FontName, Path]] = dict()
        self._in_cache = set()
        self._search_dirs = self.get_default_fonts_directories()
    
    @property
    def font_directories(self) -> List[Path]:
        return self._search_dirs
    @font_directories.setter
    def font_directories(self, value: List[str]):
        self._search_dirs = value
    
    @classmethod
    def get_font_name(cls, font: Path) -> Tuple[FontName, FontFamilyName]:
        if (isinstance(font, str)):
            if (os.path.isfile(font)):
                _path = font
            # elif (os.path.isfile('C:\\Windows\\Fonts\\' + font)):
            #     _path = os.path.isfile('C:\\Windows\\Fonts\\' + font)
            else:
                logger.error(f"Cannot open font file: {font}")
                raise FileNotFoundError
            
            logger.debug(f"Opening font {_path}")
            _font_obj = TTFont(_path)
        elif (isinstance(font, TTFont)):
            _font_obj = font
        else:
            raise TypeError(f"Font argument must be either 'str' or 'TTLFont', not '{type(font)}'.")
        
        # Get the short name from the font's names table
        name: Optional[FontName] = None
        family: Optional[FontFamilyName] = None
        for record in _font_obj['name'].names: # type: Record
            if (b'\x00' in record.string):
                name_str = record.string.decode('utf-16-be')
            else:
                name_str = record.string.decode('utf-8')
            
            if (name is None and record.nameID == cls._FONT_SPECIFIER_NAME_ID):
                name: FontName = FontName(name_str)
            elif (family is None and record.nameID == cls._FONT_SPECIFIER_FAMILY_ID):
                family: FontFamilyName = FontFamilyName(name_str)
            
            if (name is not None and family is not None):
                break
        
        logger.debug(f"Font detected: {name} from {family}")
        return name, family
    
    @classmethod
    def get_default_fonts_directories(cls) -> List[Path]:
        default = \
        [
            '.fonts',
            os.path.join(os.environ.get('HOME', ''), '.fonts'),
        ]
        
        result = default
        if (os.name == 'nt'):
            result += \
            [
                os.path.join(os.environ['WINDIR'], 'Fonts'),
            ]
        elif (os.name == 'posix'):
            result += \
            [
                '/usr/share/fonts',
                '/usr/local/share/fonts',
                '/usr/X11R6/lib/X11/fonts',
                '/usr/share/X11/fonts/Type1',
                '/usr/share/X11/fonts/OTF',
            ]
        
        return list(map(Path, result))
    
    @classmethod
    def _verify_font(cls, lower_font_name: FontName, *, bold: bool, italic: bool) -> bool:
        return (('bold' in lower_font_name) == bold) and (('italic' in lower_font_name) == italic)
    
    def find_font_file_by_fontname(self, family_name: FontFamilyName, *, bold: bool = False, italic: bool = False) -> Optional[Path]:
        if (family_name in self.cached):
            for lower_font_name in self.cached[family_name]:
                if (self._verify_font(lower_font_name, bold=bold, italic=italic)):
                    logger.debug(f"Font info restored from cache: {self.cached[family_name][lower_font_name]}")
                    return self.cached[family_name][lower_font_name]
        
        for result in self._find_and_cache(validate=True, family_name=family_name, bold=bold, italic=italic):
            logger.debug(f"Font found by direct search: {result}")
            return result
        else:
            return None
    
    def find_font_file_by_filename(self, filename: str) -> Optional[Path]:
        for font_dir in self.font_directories:
            try:
                return Path(next(glob.iglob(font_dir + '/**/' + filename, recursive=True)))
            except (StopIteration, OSError):
                pass
        
        return None
    
    def preload_font(self, filename: str, family_name: Optional[FontFamilyName] = None, *, bold: bool = False, italic: bool = False) -> Optional[Path]:
        for result in self._find_and_cache(filename, family_name, bold=bold, italic=italic, validate=True, force=True):
            return result
        else:
            return None
    
    def cache_all(self):
        for _ in self._find_and_cache(validate=False):
            pass
    
    def _find_and_cache(self, filename: str = '*', family_name: Optional[FontFamilyName] = None, **kwargs) -> Iterator[Path]:
        for font_dir in self.font_directories:
            all_fonts: Iterator[str] = glob.iglob(f'{font_dir}/**/{filename}', recursive=True)
            for font_path in all_fonts:
                font_path = Path(font_path)
                if (not os.path.isfile(font_path)):
                    continue
                
                test = self._check_font(font_path=font_path, family_name=family_name, **kwargs)
                if (test):
                    yield font_path
    
    def _check_font(self, font_path: Path, family_name: FontFamilyName, *, bold: bool, italic: bool, validate: bool, force: bool = False) -> Optional[bool]:
        if not (font_path in self._in_cache):
            logger.debug(f"Trying to open {font_path}")
            try:
                try:
                    _name, _family_name = self.get_font_name(font_path)
                except:
                    if (force):
                        logger.debug("Force preloading font", exc_info=True)
                    else:
                        raise
                
                if (force):
                    _family_name = family_name
                    _name = "{family}{bold}{italic}".format(family=family_name, bold=" Bold" if bold else "", italic=" Italic" if italic else "")
            
            except:
                logger.warning(f"Cannot parse font file: {font_path}")
                return None
            
            else:
                lower_font_name = _name.lower()
                if (not _family_name in self.cached):
                    self.cached[_family_name] = { lower_font_name: font_path }
                else:
                    self.cached[_family_name][lower_font_name] = font_path
                
                if (validate):
                    if (_family_name == family_name and self._verify_font(lower_font_name, bold=bold, italic=italic)):
                        return True
            
            self._in_cache.add(font_path)
        
        return False

_DEFAULT_FONT_FINDER: FontFinder = None
def default_font_finder() -> FontFinder:
    global _DEFAULT_FONT_FINDER
    if (_DEFAULT_FONT_FINDER is None):
        _DEFAULT_FONT_FINDER = FontFinder()
    
    return _DEFAULT_FONT_FINDER

__all__ = \
[
    'default_font_finder',
    
    'FontFamilyName',
    'FontFinder',
    'FontName',
]
