import copy
import math
from logging import getLogger
from typing import *

from PIL import ImageDraw
from PIL.ImageFont import FreeTypeFont
from dataclasses import asdict, dataclass, field, InitVar
from hammerdraw.util import Point, Region, ImageType, Size

from .enums import PrintModes, TextAlignment, CapitalizationModes
from .font import Font
from .font_finder import FontFinder, default_font_finder
from .printer import Obstacle, ParagraphObject, ParagraphLine
from .text_drawer import TextDrawer
from .text_funcs import capitalize_first, capitalize, capitalize_smart

logger = getLogger("text_drawer")

def log_time(*args, **kwargs):
    from hammerdraw.util import log_time as _log_time
    kwargs['logger'] = logger
    return _log_time(*args, **kwargs)

@dataclass
class TextDrawerImpl(TextDrawer):
    
    im: InitVar[ImageType]
    font_finder: FontFinder = field(default_factory=default_font_finder, repr=False)
    _drawer: ImageDraw.ImageDraw = field(init=False, default=None)
    _font: Font = field(init=False, default=False)
    
    @property
    def font(self):
        return self._font
    
    def __post_init__(self, im:ImageType):
        self._drawer = ImageDraw.ImageDraw(im)
        self._font = Font.default()
    
    @classmethod
    def get_default_text_finder(cls) -> FontFinder:
        return default_font_finder()
    
    def set_font(self, font: Font = None, **kwargs) -> 'TextDrawerImpl':
        if (font is not None):
            if (kwargs):
                raise ValueError("Either 'font' or 'kwargs' could be present, not both")
            else:
                self._font = font
        else:
            self._font = self.font.replace(**kwargs)
        
        if (self.font.font_finder is not self.font_finder):
            # noinspection PyArgumentList
            self._font = self.font.replace(font_finder=self.font_finder)
        
        return self
    
    def get_font(self) -> Dict[str, Any]:
        # noinspection PyDataclass
        return asdict(self.font)
    
    @log_time("Line print")
    def print_line(self, position: Point, text: str):
        logger.debug(f"Printing text line: '{text}'")
        self._print(position, text, print_mode=PrintModes.NormalPrint)
    
    @log_time("Text region print")
    def print_in_region(self, region: Region, text: str, *, offset_borders: bool = True, obstacles: List[Obstacle] = None, max_lines: Optional[int] = None) -> Size:
        logger.debug(f"Printing text region: '{text}'")
        base_font = self.font
        if (self.font.auto_adjust_scale):
            font_scale = 1.0
            font_scale_step = 0.05
            _, max_size = self._region_canonical_form(region, offset_borders)
            while font_scale > font_scale_step:
                try:
                    size = self._print_in_region(region, text, offset_borders=offset_borders, real_print=False, obstacles=obstacles, max_lines=max_lines)
                except AssertionError:
                    pass
                else:
                    if (size <= max_size): break
                
                font_scale -= font_scale_step
                self.set_font(font_size=base_font.font_size * font_scale)
        
        result = self._print_in_region(region, text, offset_borders=offset_borders, real_print=True, obstacles=obstacles, max_lines=max_lines)
        self.set_font(base_font)
        return result
    
    @log_time("Text region size")
    def get_text_size(self, region: Region, text: str, *, offset_borders: bool = True, obstacles: List[Obstacle] = None) -> Size:
        logger.debug(f"Requested region text size: '{text}'")
        result = self._print_in_region(region, text, offset_borders=offset_borders, real_print=False, obstacles=obstacles)
        return result
    
    def _region_canonical_form(self, region: Region, offset_borders: bool) -> Tuple[Point, Size]:
        if (offset_borders):
            x, y, w, h = region
        else:
            x, y, x2, y2 = region
            w = x2 - x
            h = y2 - y
        
        return (x, y), (w, h)
    
    def _print_in_region(self, region: Region, text:str, *, offset_borders: bool, real_print: bool, obstacles: List[Obstacle], max_lines: Optional[int] = None) -> Size:
        (x, y), (w, h) = self._region_canonical_form(region, offset_borders)
        
        max_width = 0
        space_width, _ = self._print__get_size(None, ' ')
        # _, line_height = self.__print(None, 'LINE HEIGHT (gyq,)', print_mode=PrintModes.GetTextSize)
        _, line_height = self._print__get_size(None, 'LINE HEIGHT')
        
        paragraphs: List[ParagraphObject] = []
        for paragraph_text in text.split('\n'):
            _horizontal_alignment = self.font.horizontal_alignment
            words = paragraph_text.split()
            paragraph_obj = ParagraphObject(horizontal_alignment=_horizontal_alignment)
            i = 0
            while (i < len (words)):
                word = words[i]
                if (word.startswith('$$')):
                    text_alignment = \
                    {
                        '$$HA_L': TextAlignment.Left,
                        '$$HA_C': TextAlignment.Center,
                        '$$HA_R': TextAlignment.Right,
                        '$$HA_J': TextAlignment.Justify,
                    }
                    if (text_alignment.get(word, None) is not None):
                        paragraph_obj.horizontal_alignment = text_alignment[word]
                    else:
                        raise KeyError(f"Unsupported operand: {word}")
                    
                    del words[i]
                
                else:
                    i += 1
            
            _lines = self._print__get_split(None, ' '.join(words), line_width=w, obstacles=obstacles)
            if (max_lines is not None):
                assert len(_lines) <= max_lines, f"Max lines attribute exceeded (max: {max_lines}, actual: {len(_lines)})."
            logger.debug(f"Splitting paragraph to lines: '{paragraph_text}' -> '{_lines}'")
            for line_text, line_width in _lines:
                paragraph_obj.lines.append(ParagraphLine(words=line_text.split(), width=line_width))
                if (max_width < line_width):
                    max_width = line_width
            
            paragraphs.append(paragraph_obj)
        
        num_lines = sum(len(paragraph_obj.lines) for paragraph_obj in paragraphs)
        total_height = int(num_lines * (1 + self.font.vertical_space_scale) * line_height + self.font.paragraph_vertical_space * (len(paragraphs) - 1))
        
        if (real_print):
            
            if (obstacles):
                _current_obstacle = 0
            
            _dh = h - total_height
            _y_offset = \
            {
                TextAlignment.Top: 0,
                TextAlignment.Bottom: _dh,
                TextAlignment.Center: _dh // 2,
            }
            _y = y + _y_offset.get(self.font.vertical_alignment, 0)
            
            for paragraph_obj in paragraphs:
                paragraph_lines = paragraph_obj.lines
                for i in range(len(paragraph_lines)):
                    line = paragraph_lines[i]
                    last_line = (i + 1 == len(paragraph_lines))
                    
                    _local_w = w
                    _obstacle = None
                    if (obstacles):
                        # noinspection PyUnboundLocalVariable
                        _obstacle = obstacles[_current_obstacle]
                        while (y >= obstacles[_current_obstacle].y2):
                            _current_obstacle += 1
                            if (_current_obstacle >= len(obstacles)):
                                obstacles = None
                                _obstacle = None
                                break
                            _obstacle = obstacles[_current_obstacle]
                    
                    if (_obstacle):
                        if ((_y <= _obstacle.y1 <= y + h) or (_obstacle.y1 <= _y <= _obstacle.y2)):
                            _local_w = _obstacle.x
                    
                    new_space_width = space_width
                    
                    def _justify():
                        if (not last_line and len(line.words) > 0):
                            num_spaces = len(line.words) - 1
                            if (num_spaces > 0):
                                nonlocal new_space_width
                                new_space_width = space_width + (_local_w - line.width) / num_spaces
                        
                        return 0
                    
                    _x_offset = \
                    {
                        TextAlignment.Right: lambda: _local_w - line.width,
                        TextAlignment.Center: lambda: (_local_w - line.width) // 2,
                        TextAlignment.Justify: _justify,
                    }
                    _x = x + _x_offset.get(paragraph_obj.horizontal_alignment, lambda: 0)()
                    
                    self._print((_x, _y), ' '.join(line.words), print_mode=PrintModes.NormalPrint, restricted_space_width=new_space_width)
                    _y += (1 + self.font.vertical_space_scale) * self.font.font_size
                _y += self.font.paragraph_vertical_space
        
        assert max_width <= w, "Max width exceeded"
        return max_width, total_height
    
    
    def _print__get_split(self, *args, **kwargs) -> List[Tuple[str, int]]:
        return self._print(*args, print_mode=PrintModes.SplitLines, **kwargs)[0]
    
    def _print__get_size(self, *args, **kwargs) -> Tuple[int, int]:
        return self._print(*args, print_mode=PrintModes.GetTextSize, **kwargs)[1]
    
    def _print__get_small_caps_font(self) -> Tuple[FreeTypeFont, int]:
        _new_size = int(self.font.font_size * 0.75)
        _char = 'ixz'
        _char = _char.upper()
    
        _font = self.font.font_base
        # noinspection PyArgumentList
        small_caps_font: FreeTypeFont = self.font.replace(font_size=_new_size).font_base
        _, _y1 = self._drawer.textsize(_char, font=small_caps_font)  # type: int, int
        _, _y2 = self._drawer.textsize(_char, font=_font)  # type: int, int
        small_caps_dy = _y2 - _y1
        
        return small_caps_font, small_caps_dy
    
    def _print__check_subsequence(self, text: str, position: int, max_length: int, char: str, subsequence_length: int = 2) -> bool:
        if (position + subsequence_length > max_length):
            return False
        elif (text[position:position + subsequence_length] != char * subsequence_length):
            return False
        elif (position > 0 and text[position - 1] == char):
            return False
        elif (position + subsequence_length < max_length and text[position + subsequence_length] == char):
            return False
        else:
            return True
    
    # noinspection PyUnboundLocalVariable
    def _print(self, position: Optional[Point], text: str, print_mode: PrintModes, restricted_space_width: int = None, debug_console_print: bool = False, **kwargs) \
        -> Tuple \
        [
            Optional[List[Tuple[str, int]]],
            Optional[Tuple[int, int]],
        ]:
        
        x, y = position or (0, 0) # type: float, float
        max_height = 0
        
        capitalize_func = \
        {
            CapitalizationModes.UpperCase: str.upper,
            CapitalizationModes.LowerCase: str.lower,
            CapitalizationModes.Capitalize: capitalize,
            CapitalizationModes.CapitalizeSmart: capitalize_smart,
            CapitalizationModes.CapitalizeFirst: capitalize_first,
        }
        _text = capitalize_func.get(self.font.capitalization, copy.copy)(text)
        
        if (print_mode == PrintModes.SplitLines):
            _line_splits: List[Tuple[str, int]] = list()
            _line_start = 0
            _line_start_x = 0
            _line_width: Optional[int] = kwargs.get('line_width', None)
            x = 0
            _word_start = 0
            _word_start_x = 0
            _last_word_end = 0
            _last_word_end_x = 0
            if (_line_width is None):
                raise ValueError("'line_width' argument is required for the SplitLines mode")
            
            _obstacles: Optional[List[Obstacle]] = kwargs.get('obstacles', None)
            _current_obstacle = 0
        
        original_font = self.font
        _continue = False
        
        if (self.font.capitalization == CapitalizationModes.SmallCaps):
            small_caps_font, small_caps_dy = self._print__get_small_caps_font()
        
        _len = len(text)
        for i in range(_len):
            if (_continue):
                _continue = False
                
                if (self.font.capitalization == CapitalizationModes.SmallCaps):
                    small_caps_font, small_caps_dy = self._print__get_small_caps_font()
                continue
            
            if (i + 2 <= _len):
                if (self._print__check_subsequence(_text, i, _len, '_')):
                    logger.debug(f"Change font style to { 'non-' * self.font.italic }italic")
                    self.set_font(italic=not self.font.italic)
                    _continue = True
                    continue
                if (self._print__check_subsequence(_text, i, _len, '*')):
                    logger.debug(f"Change font style to { 'non-' * self.font.bold }bold")
                    self.set_font(bold=not self.font.bold)
                    _continue = True
                    continue
            
            _char = _text[i]
            _x = int(x)
            _y = int(y)
            _font = self.font.font_base
            
            if (self.font.capitalization == CapitalizationModes.SmallCaps and not _char.isupper()):
                _char = _char.upper()
                _y += small_caps_dy
                _font = small_caps_font
            w, h = self._drawer.textsize(_char, font=_font) # type: int, int
            if (print_mode == PrintModes.NormalPrint):
                if (debug_console_print):
                    print(text[i], end='')
                self._drawer.text((_x, _y), _char, self.font.color, font=_font)
            if (_char.isspace() or i == _len - 1):
                _space_width: float = restricted_space_width or w * self.font.space_scale
                
                if (print_mode == PrintModes.SplitLines):
                    
                    _local_line_width = _line_width
                    _obstacle = None
                    if (_obstacles):
                        _obstacle = _obstacles[_current_obstacle]
                        while (y >= _obstacles[_current_obstacle].y2):
                            _current_obstacle += 1
                            if (_current_obstacle >= len(_obstacles)):
                                _obstacles = None
                                _obstacle = None
                                break
                            _obstacle = _obstacles[_current_obstacle]
                    
                    if (_obstacle):
                        if ((y <= _obstacle.y1 <= y + h) or (_obstacle.y1 <= y <= _obstacle.y2)):
                            _local_line_width: int = _obstacle.x
                    
                    _local_line_width -= _space_width
                    
                    if ((x - _line_start_x) > _local_line_width):
                        _current_line_width = _last_word_end_x - _line_start_x
                        logger.debug("Line shift required after '%s'", text[_line_start:_last_word_end])
                        assert _current_line_width <= _local_line_width, "Line width exceeded"
                        _line_splits.append((text[_line_start:_last_word_end], _current_line_width))
                        _line_start = _word_start
                        _line_start_x = _word_start_x
                        y += max_height
                        max_height = 0
                    elif (i > 1 and _text[i - 1] != ' '):
                        _last_word_end = i
                        _last_word_end_x = x
                
                x += _space_width
                _word_start = i + 1
                _word_start_x = x
            
            else:
                x += w + self.font.character_separator_scale * self.font.font_size
            if (max_height < h):
                max_height = h
        
        initial_x, _ = position or (0, 0) # type: int, int
        if (debug_console_print):
            print('', end='\n')
        self.set_font(original_font)
        
        if (print_mode == PrintModes.SplitLines):
            _current_line_width = x - _line_start_x
            assert _current_line_width <= _line_width, "Last line width exceeded"
            _line_splits.append((text[_line_start:], math.ceil(_current_line_width)))
            return _line_splits, None
        else:
            return None, (math.ceil(x - initial_x), max_height)

__all__ = \
[
    'TextDrawerImpl',
]
