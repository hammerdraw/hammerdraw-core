from abc import ABC
from logging import getLogger
from typing import *

from hammerdraw.util import Point, Region, Size
from .font import Font
from .font_finder import FontFinder
from .printer import Obstacle

logger = getLogger("text_drawer")

def log_time(*args, **kwargs):
    from hammerdraw.util import log_time as _log_time
    kwargs['logger'] = logger
    return _log_time(*args, **kwargs)

T = TypeVar('T')
class TextDrawer(ABC):
    
    @property
    def font(self) -> Font:
        raise NotImplementedError
    
    @classmethod
    def get_default_text_finder(cls) -> FontFinder:
        raise NotImplementedError
    
    @overload
    def set_font(self: T, font: Font) -> T:
        pass
    @overload
    def set_font(self: T, **kwargs) -> T:
        pass
    def set_font(self: T, *args, **kwargs) -> T:
        raise NotImplementedError
    
    def get_font(self) -> Dict[str, Any]:
        raise NotImplementedError
    
    def print_line(self, position: Point, text: str):
        raise NotImplementedError
    
    def print_in_region(self, region: Region, text: str, *, offset_borders: bool = True, obstacles: List[Obstacle] = None, max_lines: Optional[int] = None) -> Size:
        raise NotImplementedError
    
    def get_text_size(self, region: Region, text: str, *, offset_borders: bool = True, obstacles: List[Obstacle] = None) -> Size:
        raise NotImplementedError

__all__ = \
[
    'TextDrawer',
]
