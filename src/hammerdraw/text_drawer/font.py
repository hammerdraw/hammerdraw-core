from typing import *

from PIL.ImageFont import FreeTypeFont, truetype
from dataclasses import field
from dataclasses_json import LetterCase
from lazy_property import LazyProperty

from dataclasses_config import *
from hammerdraw.util import get_color
from .enums import TextAlignment, CapitalizationModes
from .font_finder import FontFinder, default_font_finder

@deserialize_with(ConstructorDataType.Any)
class FontColor(Tuple[int, ...]):
    
    @overload
    def __new__(cls, c: List[int]):
        pass
    @overload
    def __new__(cls, c: Tuple[int, ...]):
        pass
    @overload
    def __new__(cls, c: int):
        pass
    @overload
    def __new__(cls, c: str):
        pass
    
    def __new__(cls, c ):
        return get_color(c)

@main_config(env_variable_name=None, reference_config_name='default-font.conf', log_config=False)
@dataclass_json_config(letter_case=LetterCase.CAMEL)
class Font(MainConfig):
    font_family: str
    font_size: float
    
    color: str
    bold: bool
    italic: bool
    horizontal_alignment: TextAlignment
    vertical_alignment: TextAlignment
    capitalization: CapitalizationModes
    
    character_width_scale: float
    space_scale: float
    vertical_space_scale: float
    paragraph_vertical_space: float
    character_separator_scale: float
    auto_adjust_scale: bool
    
    font_finder: FontFinder = field(default_factory=default_font_finder, repr=False, compare=False)
    
    @LazyProperty
    def font_base(self) -> FreeTypeFont:
        return truetype(font=self.font_file, size=int(self.font_size), encoding='unic')
    
    @LazyProperty
    def font_name_repr(self) -> str:
        parts = [ self.font_family ]
        if (self.bold): parts.append('Bold')
        if (self.italic): parts.append('Italic')
        
        return ', '.join(parts)
    
    @LazyProperty
    def font_file(self) -> Path:
        _test_font = self.font_finder.find_font_file_by_fontname(family_name=self.font_family, bold=self.bold, italic=self.italic)
        if (_test_font):
            return _test_font
        else:
            raise FileNotFoundError(f"Requested font {self.font_name_repr!r} is not installed.")

__all__ = \
[
    'Font',
]
