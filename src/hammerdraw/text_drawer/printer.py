from dataclasses import dataclass, field
from typing import *

from PIL.ImageDraw import ImageDraw

from .enums import TextAlignment

@dataclass
class Obstacle:
    x: int
    y1: int
    y2: int

@dataclass
class ParagraphLine:
    words: List[str]
    width: int

@dataclass
class ParagraphObject:
    horizontal_alignment: TextAlignment
    lines: List[ParagraphLine] = field(default_factory=list)

class Printer:
    drawer: ImageDraw
    color: tuple
    font: Any
    
    _text: str
    _lines: List[Tuple[str, int]]
    
    def print_simple(self, text: str):
        pass
    def print_split(self, text: str):
        pass
    
    def _print_text(self, position: Tuple[int, int], c: str):
        self.drawer.text(position, c, self.color, font=self.font)
    
    def _get_text_size(self, c: str):
        w, h = self.drawer.textsize(c, font=self.font)
        return w, h

__all__ = \
[
    'Obstacle',
    'ParagraphLine',
    'ParagraphObject',
    'Printer',
]
