from typing import *

from dataclasses import dataclass

from dataclasses_config import *
from .text_drawer import TextDrawer

@dataclass(frozen=Settings.frozen)
class TextDrawerConfig(Config):
    font_directories: List[Path]
    font_preloads: List[Dict[str, Union[str, bool]]]
    text_drawer_class: DynamicClass[TextDrawer]
    
    def __post_init__(self):
        for i in range(len(self.font_directories)):
            self.font_directories[i] = Path(self.font_directories[i])
        super().__post_init__()

__all__ = \
[
    'TextDrawerConfig',
]
