import datetime
import threading
import warnings
from collections import OrderedDict
from typing import *

from PIL import Image
from lazy_property import LazyProperty

from hammerdraw.compilers import ModuleCompiler, CompilerError, CompilerWarning
from hammerdraw.interfaces import ILoggable, IModule
from hammerdraw.text_drawer import TextDrawer, Font
from hammerdraw.util import ImageType, dict_filter_out
from .module_attribute import ModuleAttribute, _DEFAULT_VALUE__MA

_DEFAULT_VALUE__MB = object()
class ModuleBase(IModule, ILoggable):
    
    # These ones MUST be overwritten
    module_name: str = None
    
    # These ones CAN be overwritten
    human_readable_name: str = None
    module_config_prefix: str = 'modules/'
    update_timeout: float = 0
    update_delay: float = 0
    
    # These ones MUST NOT be overwritten
    index: int = None
    parent: ModuleCompiler = None
    parent_type: Type[ModuleCompiler] = None
    module_attributes: Dict[str, ModuleAttribute] = OrderedDict() # class attribute
    
    width: int = None
    height: int = None
    top_offset: int = None
    compiled: Optional[ImageType] = None
    after_cont = None
    
    def __init__(self, parent: ModuleCompiler, index: int, **kwargs):
        self.parent = parent
        self.parent_type = getattr(parent, 'compiler_type', None) or 'unknown'
        self.logger_name = getattr(self, 'logger_name', None) or f"{self.parent.logger_name}.{self.module_name}_module"
        
        super().__init__()
        
        self.index = index
        self.top_offset = 0
        try:
            self.initialize(**kwargs)
        except (TypeError, ValueError) as e:
        # except (ConnectionError) as e:
            raise type(e)(f"Cannot initialize module '{type(self).__name__}: {self.module_name}': {e}")
        
        self.human_readable_name = self.human_readable_name or self.module_name.replace('_', ' ').capitalize()
        
        # Try config:
        if (parent.get_from_config(self.module_config_path) is None):
            message = f"Config not found for module {self.parent_type}/{self.module_name} at {self.module_config_path!r}"
            self.logger.error(message)
            raise CompilerError(message)
    
    @LazyProperty
    def module_config_path(self) -> str:
        return self.module_config_prefix + self.module_name
    
    @LazyProperty
    def text_drawer_class(self) -> Type[TextDrawer]:
        return self.parent.text_drawer_class
    
    def initialize(self, *args, **kwargs):
        self.init_attributes(kwargs)
        self._initialize(**kwargs)
    
    def _initialize(self, after=None):
        self.after_cont = after
    
    def init_attributes(self, kwargs: Dict[str, Any]):
        for _attr_name, _attr_obj in self.module_attributes.items():
            _attr_obj.initialize(self, kwargs.pop(_attr_name, _DEFAULT_VALUE__MA))
    
    def get_from_module_config(self, key):
        warnings.warn("get_from_module_config() method is going to be deprecated, use get_from_config() instead.", DeprecationWarning, 2)
        return self.get_from_config(key)
    
    def get_from_config(self, key, default=None):
        if (key):
            path = f'{self.module_config_path}/{key}' if (self.module_config_path) else key
        else:
            path = self.module_config_path
        
        return self.parent.get_from_config(path, default=default)
    
    def get_from_raw(self, key: str, default=_DEFAULT_VALUE__MB):
        return self.parent.get_from_raw(key, default=default)
    
    def load_style(self, style_name: str):
        style = self.parent.get_from_config(f'styles/{style_name}')
        if (style is None):
            raise ValueError(f"Cannot load style '{style_name}'!")
        return self.load_font(style)
    
    def load_font(self, obj: Dict[str, Any], font_prefix=None, style_prefix=None):
        font_prefix = font_prefix or 'font'
        style_prefix = style_prefix or 'style'
        
        style = obj.get(style_prefix, None)
        base = self.load_style(style) if (style) else Font.default()
        font = Font.from_dict(obj.get(font_prefix, dict()), infer_missing=True)
        _font_dict = font.asdict()
        _font_dict = dict_filter_out(_font_dict)
        return base.replace(**_font_dict)
    
    def get_text_drawer(self, base:ImageType, font_prefix: str = None, style_prefix: str = None) -> TextDrawer:
        td = self.text_drawer_class(base) # type: TextDrawer
        obj = self.get_from_config('')
        font = self.load_font(obj, font_prefix=font_prefix, style_prefix=style_prefix)
        td.set_font(font)
        return td
    
    def parse_offset(self, x):
        _top_offset = 0
        
        skip_kwd = False
        if (isinstance(x, str)):
            if (x.startswith('skip:')):
                _top_offset = int(x[5:])
                skip_kwd = True
        else:
            _top_offset = x
        
        return _top_offset, skip_kwd
    
    def get_top_offset(self):
        _top_offset = 0
        if (self.get_from_config('continuous')):
            if (self.after_cont):
                _top_offset, skip_kwd = self.parse_offset(self.parent.continuous_print[self.after_cont])
                if (not skip_kwd):
                    _top_offset += int(self.get_from_config('offset') * self.parent.get_from_raw('separatorScale', 1.0))
        
        self.top_offset = _top_offset
        return _top_offset
    
    def get_size(self) -> Tuple[int, int]:
        _width = self.get_from_config('width')
        _height = self.get_from_config('height')
        
        _height -= self.get_top_offset()
        
        
        size = (_width, _height)
        if (_height < 0 or _width < 0):
            raise CompilerError(f"Module '{self.human_readable_name}': cannot initialize size {size}")
        return size
    
    def get_position(self) -> Tuple[int, int]:
        positionX = self.get_from_config('positionX')
        positionY = self.get_from_config('positionY')
        
        old_offset = self.top_offset
        positionY += self.get_top_offset()
        if (old_offset != self.top_offset):
            x, skip = self.parse_offset(self.parent.continuous_print[self.module_name])
            x += self.top_offset - old_offset
            if (skip):
                self.parent.continuous_print[self.module_name] = "skip:{0}".format(x)
            else:
                self.parent.continuous_print[self.module_name] = x
        
        position = (positionX, positionY)
        return position
    
    def prepare(self):
        _size = self.get_size()
        base = Image.new('RGBA', _size, 0x00ffffff)
        self.width, self.height = _size
        return base
    
    def compile(self) -> Optional[ImageType]:
        _now = datetime.datetime.now()
        base = self.prepare()
        actual_height = "skip:0"
        try:
            self.get_top_offset()
            actual_height = self._compile(base)
            if (actual_height == 0):
                actual_height = "skip:{0}".format(self.top_offset)
            else:
                actual_height += self.top_offset
        
        except CompilerWarning as warn:
            self.logger.debug("Module {name} compiled with warnings. Time spent: {dt}ms".format(name=self.module_name, dt=(datetime.datetime.now() - _now).total_seconds() * 1000))
            self.logger.warning(warn)
            thr = threading.Thread(target=self._on_warn, args=(warn,), kwargs={ })
            thr.start()
        else:
            self.logger.debug("Module {name} compiled. Time spent: {dt}ms".format(name=self.module_name, dt=(datetime.datetime.now() - _now).total_seconds() * 1000))
        
        self.parent.continuous_print[self.module_name] = actual_height
        self.compiled = base
        return base
    def _compile(self, base:Image):
        raise NotImplementedError
    
    def insert(self, parent_base:Image.Image):
        if (self.compiled is None):
            raise CompilerError("Cannot insert non-compiled module")
        
        _x, _y = self.get_position()
        if (_y < 0):
            _y = parent_base.height + _y - self.compiled.height
        _image = self.compiled.convert('RGB')
        _mask = self.compiled
        parent_base.paste(_image, (_x, _y), _mask)
    
    def update(self):
        raise NotImplementedError
    
    def _on_update(self, *args, **kwargs):
        pass
    
    def _on_warn(self, *args, **kwargs):
        pass

__all__ = \
[
    'ModuleBase',
]
