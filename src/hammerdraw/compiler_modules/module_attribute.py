from typing import Any, Type, Callable, Union

from camel_case_switcher import underscore_to_camel_case, camel_case_to_underscore

from hammerdraw.interfaces import IModule
from hammerdraw.util import GetterType, SetterType

_DEFAULT_VALUE__MA = object()

class ModuleAttribute:
    # Meta
    getter: GetterType
    setter: SetterType
    property_value_attribute_name: str
    property_name: str
    
    # Properties
    name: str
    key: str
    default: Any
    
    # Flags
    mandatory: bool
    allow_value_from_config: bool
    allow_value_from_raw: bool
    
    def __init__(self, name: str, default: Union[Callable, Any] = None, key: str = None, *, property_name: str = None, mandatory: bool = False, allow_value_from_config: bool = False, allow_value_from_raw: bool = True, getter: GetterType = None, setter: SetterType = None):
        """
        
        :param name: str
        :param default: 
        :param key: 
        :param mandatory: 
        :param allow_value_from_config: 
        :param allow_value_from_raw: 
        :param getter: 
        :param setter: 
        """
        self.name = camel_case_to_underscore(name)
        self.default = default
        
        self.mandatory = mandatory
        self.allow_value_from_config = allow_value_from_config
        self.allow_value_from_raw = allow_value_from_raw
        
        self.property_value_attribute_name = f'_{name}__value'
        if (key is None):
            key = underscore_to_camel_case(self.name)
        self.key = key
        if (property_name is None):
            property_name = self.name.replace('/', '_')
        self.property_name = property_name
        
        if (getter is None):
            def getter(_self: IModule):
                _value = getattr(_self, self.property_value_attribute_name)
                if (_value is not _DEFAULT_VALUE__MA):
                    return _value
                if (self.allow_value_from_raw):
                    _value = _self.get_from_raw(self.key, default=_DEFAULT_VALUE__MA)
                    if (_value is not _DEFAULT_VALUE__MA):
                        return _value
                if (self.allow_value_from_config):
                    _value = _self.get_from_config(self.key, default=_DEFAULT_VALUE__MA)
                    if (_value is not _DEFAULT_VALUE__MA):
                        return _value
                _value = self.default
                if (callable(_value)):
                    _value = _value(_self)
                return _value
        self.getter = getter
        
        if (setter is None):
            def setter(_self: IModule, value):
                setattr(_self, self.property_value_attribute_name, value)
        self.setter = setter
    
    def initialize(self, obj: IModule, value = _DEFAULT_VALUE__MA):
        if (value is not _DEFAULT_VALUE__MA):
            setattr(obj, self.property_value_attribute_name, value)
        elif (self.mandatory):
            self._error("Attribute is mandatory", module_name=obj.module_name)
    
    def apply_to_class(self, cls: Type[IModule]):
        setattr(cls, self.property_value_attribute_name, _DEFAULT_VALUE__MA)
        setattr(cls, self.property_name, property(self.getter, self.setter))
        cls.module_attributes[self.name] = self
    
    def _error(self, msg: str, cls=AttributeError, *, module_name: str = None):
        base_msg = f"Cannot initialize attribute '{self.name}'"
        module_name_msg = f" for module '{module_name}'" if (module_name) else ''
        raise cls(f"{base_msg}{module_name_msg}: {msg}")

def module_attribute(*args, **kwargs):
    def decorator(cls: Type[IModule]):
        attribute = ModuleAttribute(*args, **kwargs)
        attribute.apply_to_class(cls)
        return cls
    return decorator

__all__ = \
[
    'module_attribute',
    'ModuleAttribute',
]
