from abc import ABC
from typing import *

from hammerdraw.util import ImageType

class ICompilable(ABC):
    def compile(self) -> Optional[ImageType]:
        raise NotImplementedError
    def update(self):
        raise NotImplementedError
    
    def get_from_config(self, key: str, default = None) -> Any:
        raise NotImplementedError


__all__ = \
[
    'ICompilable',
]
