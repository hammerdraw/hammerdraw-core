from abc import ABC
from logging import Logger, getLogger

class ILoggable(ABC):
    logger: Logger
    logger_name: str
    
    def __init__(self, *args, **kwargs):
        self.init_logger()
        super().__init__(*args, **kwargs)
    
    def init_logger(self):
        if (getattr(self, 'logger_name', None) is None):
            raise ValueError(f"Logger name is not set up")
        
        self.logger = getLogger(self.logger_name)

__all__ = \
[
    'ILoggable',
]
