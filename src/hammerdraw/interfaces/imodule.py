from abc import ABC
from typing import *

from hammerdraw.text_drawer import TextDrawer
from hammerdraw.util import ImageType
from .icompilable import ICompilable

class IModule(ICompilable, ABC):
    
    # These ones MUST be overwritten
    module_name: str
    
    # These ones CAN be overwritten
    human_readable_name: str
    module_config_prefix: str
    update_timeout: float
    update_delay: float
    
    # These ones SHOULD NOT be overwritten    
    width: int = None
    height: int = None
    top_offset: int = None
    compiled: Optional[ImageType] = None
    module_attributes: dict = None
    
    @property
    def module_config_path(self) -> str:
        raise NotImplementedError
    
    @property
    def text_drawer_class(self) -> Type[TextDrawer]:
        raise NotImplementedError
    
    def initialize(self, *args, **kwargs):
        raise NotImplementedError
    
    T = TypeVar('T')
    def get_from_raw(self, key: str, default: T = None) -> T:
        raise NotImplementedError
    del T
    
    def get_top_offset(self) -> int:
        raise NotImplementedError
    
    def get_size(self) -> Tuple[int, int]:
        raise NotImplementedError
    
    def get_position(self) -> Tuple[int, int]:
        raise NotImplementedError
    
    def prepare(self):
        raise NotImplementedError
    
    def compile(self) -> Optional[ImageType]:
        raise NotImplementedError
    
    def insert(self, parent_base: ImageType):
        raise NotImplementedError
    
    def _on_update(self, *args, **kwargs):
        pass
    
    def _on_warn(self, *args, **kwargs):
        pass

__all__ = \
[
    'IModule',
]
