from abc import ABC
from typing import *

from dataclasses_config import Path
from hammerdraw.text_drawer import TextDrawer, CapitalizationModes
from hammerdraw.util import Point, Region, ImageType, RawType, ColorType

class GradientPart:
    position: float
    color: ColorType

class IImageOperator(ABC):
    def insert_table \
    (
        self,
        vertical_columns: List[int],
        top: int,
        cell_height: int,
        data: RawType,
        *,
        
        body_row_template: List[str],
        body_text_drawer: TextDrawer,
        body_row_interval: int,
        body_capitalization: Union[CapitalizationModes, str, None] = None,
        body_bold: Optional[bool] = None,
        body_italic: Optional[bool] = None,
        
        header_row: List[str] = None,
        header_text_drawer: TextDrawer = None,
        header_row_interval: int = None,
        header_capitalization: Union[CapitalizationModes, str, None] = None,
        header_bold: Optional[bool] = None,
        header_italic: Optional[bool] = None,
    ):
        raise NotImplementedError
    
    def insert_image_scaled \
    (
        self,
        base_image: ImageType,
        region: Region,
        image_path: Union[Path, ImageType],
        *,
        offset_borders: bool = True,
        scale_func: Callable[[float, float], float] = max,
        foreground: bool = False,
        center: bool = False,
    ):
        raise NotImplementedError
    
    def get_image_size(self, image_path: Path) -> Point:
        raise NotImplementedError
    
    def insert_image_centered(self, base_image: ImageType, position: Point, image_path: Path, *, offset_borders: bool = True, scale: float = 1.0) -> Region:
        raise NotImplementedError
    
    def get_gradient_base(self, width: int, gradient: List[GradientPart]) -> ImageType:
        raise NotImplementedError

__all__ = \
[
    'GradientPart',
    'IImageOperator',
]
