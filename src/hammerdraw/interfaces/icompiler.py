from abc import ABC
from typing import Iterable, Optional

from hammerdraw.util import RawType, ImageType
from .icompilable import ICompilable

class ICompiler(ICompilable, ABC):
    raw: RawType = None
    compiler_type: str
    
    schema_path: str
    template_path: str
    output_directory: str
    
    output_name: str = None
    filename: str = None
    compiled_filename: str = None
    
    def search(self) -> Iterable[str]: pass
    def find(self, name: str) -> Optional[str]: pass
    def validate_raw(self, raw: RawType) -> bool: pass
    def create(self): pass
    def open(self, name: str) -> Optional[RawType]: pass
    def save_raw(self) -> Optional[str]: pass
    def save_compiled(self) -> Optional[str]: pass
    
    def generate_filename(self) -> str: pass
    def get_output_name(self) -> Optional[str]: pass
    def get_output_directory(self) -> str: pass
    
    def prepare_base(self) -> Optional[ImageType]: pass
    def join(self, base: ImageType) -> bool: pass

__all__ = \
[
    'ICompiler',
]
