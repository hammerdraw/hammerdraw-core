import warnings
from abc import ABC
from typing import *

from dataclasses_config import Config

T = TypeVar('T', bound=Config)
class IConfigurable(ABC, Generic[T]):
    config: T
    
    def __init__(self, config: T, *args, **kwargs):
        self.config = config
        super().__init__(*args, **kwargs)
    
    def get_from_config(self, key: str, default = None) -> Any:
        warnings.warn("This method is going to be deprecated, use 'self.config.field_name' instead", DeprecationWarning, 2)
        return self.config._config.get(key, default)

__all__ = \
[
    'IConfigurable',
]
