from typing import *

from .imodule import IModule

ModuleDeclaration = Union \
[
    Type[IModule],
    Tuple[Type[IModule], Union[list, tuple, Dict[str, Any]]],
    # Tuple[Type[IModule], Any, ...],
]
""" A type alias representing a possible module declaration """

ModulesIndexType = int
""" A type alias representing a key-type for modules container """

__all__ = \
[
    'ModuleDeclaration',
    'ModulesIndexType',
]
