from typing import *

T = TypeVar('T')
def join_list(lst: List[T], separator:str= ', ', last_separator: str = None, formatter: Union[str, Callable[[T], str]] = '{}'.format) -> str:
    if (not lst):
        return ''
    
    if (last_separator is None):
        last_separator = separator
    
    if (isinstance(formatter, str)):
        formatter = formatter.format
    list_str = [ formatter(x) for x in lst ]
    
    if (len(lst) == 1):
        result = list_str[0]
    else:
        result = f'{separator.join(list_str[:-1])}{last_separator}{list_str[-1]}'
    
    return result

__all__ = \
[
    'join_list',
]
