class TextToRawException(Exception):
    def __init__(self, *args):
        super().__init__(*args)

class CannotCreateTemplateException(TextToRawException):
    def __init__(self):
        super().__init__("Cannot create template data")

class CannotParseTextException(TextToRawException):
    def __init__(self):
        super().__init__("Invalid text data provided")

class InvalidRawException(TextToRawException):
    def __init__(self):
        super().__init__("Text data parsed successfully, but the raw data is invalid")

__all__ = \
[
    'TextToRawException',
    'CannotCreateTemplateException',
    'CannotParseTextException',
    'InvalidRawException',
]
