import re
import sre_parse
from functools import partial
from sre_constants import CATEGORY, AT, MAXREPEAT, NEGATE, AT_BEGINNING, AT_END
from sre_constants import SUBPATTERN, MIN_REPEAT, MAX_REPEAT, LITERAL, NOT_LITERAL, IN, BRANCH, ANY
from sre_parse import SubPattern
from typing import Pattern, Union, List, Tuple, Optional, Dict, Callable, Any

def find_subpattern_multi(possible_parents: List[SubPattern], index: int) -> SubPattern:
    for pat in possible_parents:
        try:
            return find_subpattern(pat, index)
        except StopIteration:
            pass
    raise StopIteration
def find_subpattern(pat: SubPattern, index: int) -> SubPattern:
    possible_parents: List[SubPattern] = list()
    for i, (t, d) in enumerate(pat.data):
        if (t is SUBPATTERN):
            group, _add_flags, _del_flags, p = d # type: Optional[int], int, int, SubPattern
            if (not (isinstance(group, int))):
                possible_parents.append(p)
            elif (group < index):
                possible_parents = [ p ]
            elif (group == index):
                return p
            elif (group > index):
                return find_subpattern_multi(possible_parents, index)
        elif (t is MAX_REPEAT or t is MIN_REPEAT):
            _min, _max, p = d # type: int, int, SubPattern
            possible_parents.append(p)
    
    return find_subpattern_multi(possible_parents, index)

CompileSubPatternFuncType = Callable[[SubPattern], str]

def _decompile__literal(literal, **kwargs) -> str:
    result = chr(literal)
    if (result in sre_parse.SPECIAL_CHARS):
        result = '\\' + result
    return result
def _decompile__not_literal(literal, **kwargs) -> str:
    return rf'[^{_decompile__literal(literal, **kwargs)}]'
def _decompile__subpattern(d: Tuple[Optional[int], int, int, SubPattern], *, compile_subpattern_func: CompileSubPatternFuncType, rev_group_indices: Dict[int, str], **kwargs) -> str:
    group, add_flags, del_flags, p = d
    result = ''
    group_prefix = '('
    group_suffix = ')'
    if (isinstance(group, int)):
        group_name = rev_group_indices.get(group, None)
        if (group_name is not None):
            group_prefix += rf'?P<{group_name}>'
    elif (group is None):
        group_prefix += '?:'
    
    result += group_prefix
    result += compile_subpattern_func(p, suppress_brackets=True)
    result += group_suffix
    return result
def _decompile__max_repeat(d: Tuple[int, int, SubPattern], *, compile_subpattern_func: CompileSubPatternFuncType, **kwargs) -> str:
    min, max, p = d
    result = ''
    if (max is MAXREPEAT):
        if (min == 0):
            suffix = '*'
        elif (min == 1):
            suffix = '+'
        else:
            suffix = f'{{{min},}}'
    elif (min == 0 and max == 1):
        suffix = '?'
    else:
        suffix = f'{{{min},{max}}}'
    
    _data = compile_subpattern_func(p)
    if (len(p.data) == 1):
        result += _data
    else:
        result += fr'(?:{_data})'
    result += suffix
    return result
def _decompile__min_repeat(d: Tuple[int, int, SubPattern], *, compile_subpattern_func: CompileSubPatternFuncType, **kwargs) -> str:
    return _decompile__max_repeat(d, compile_subpattern_func=compile_subpattern_func, **kwargs) + '?'
def _decompile__in(lst: List[Tuple[int, Any]], **kwargs) -> str:
    _data = _decompile_data(lst, **kwargs)
    if (len(lst) == 1):
        return _data
    else:
        return fr'[{_data}]'
def _decompile__branch(d: Tuple[None, List[SubPattern]], *, compile_subpattern_func: CompileSubPatternFuncType, suppress_brackets: bool = False, **kwargs) -> str:
    _, lst = d
    _data = '|'.join(compile_subpattern_func(l) for l in lst)
    if (suppress_brackets):
        return _data
    else:
        return fr'(?:{_data})'
def _decompile__category(category: int, **kwargs) -> str:
    return CATEGORIES_REV[category]
def _decompile__at(at: int, **kwargs) -> str:
    return AT_REV[at]
def _decompile__any(_: None, **kwargs) -> str:
    return '.'
def _decompile__negate(_: None, **kwargs) -> str:
    return '^'

CATEGORIES_REV: Dict[int, str] = { d[0][1]: symbol for symbol, (t, d) in sre_parse.CATEGORIES.items() if t is IN }
AT_REV: Dict[int, str] = { d: symbol for symbol, (t, d) in sre_parse.CATEGORIES.items() if t is AT }
AT_REV[AT_BEGINNING] = '^'
AT_REV[AT_END] = '$'

COMPILE_FUNCTIONS: Dict[Callable, str] = \
{
    LITERAL: _decompile__literal,
    NOT_LITERAL: _decompile__not_literal,
    SUBPATTERN: _decompile__subpattern,
    MAX_REPEAT: _decompile__max_repeat,
    MIN_REPEAT: _decompile__min_repeat,
    IN: _decompile__in,
    BRANCH: _decompile__branch,
    
    CATEGORY: _decompile__category,
    AT: _decompile__at,
    ANY: _decompile__any,
    NEGATE: _decompile__negate,
}

def _decompile_data(data: List[Tuple[int, Any]], **kwargs) -> str:
    result = ''
    for i, (t, d) in enumerate(data):
        _func = COMPILE_FUNCTIONS.get(t, None)
        if (_func is not None):
            result += _func(d, **kwargs)
        else:
            raise NotImplementedError(f"Unsupported type: {t}")
    return result

def decompile_subpattern(pat: SubPattern, *, rev_group_indices: Dict[int, str] = None, **kwargs) -> str:
    if (rev_group_indices is None):
        rev_group_indices = { name: index for index, name in  pat.pattern.groupdict.items() }
    
    _decompile: Callable[[SubPattern], str] = partial(decompile_subpattern, rev_group_indices=rev_group_indices)
    return _decompile_data(pat.data, compile_subpattern_func=_decompile, rev_group_indices=rev_group_indices, **kwargs)

def _get_group_pattern(regex: Pattern[str], index: int) -> Pattern:
    p = sre_parse.parse(regex.pattern)
    _data = find_subpattern(p, index)
    _decompiled = decompile_subpattern(_data)
    compiled = re.compile(_decompiled, regex.flags)
    return compiled

_cache: Dict[Tuple[str, int], Pattern] = dict()
def get_group_pattern(regex: Pattern[str], group: Union[str, int]) -> Pattern:
    index = regex.groupindex[group] if (isinstance(group, str)) else group
    _cache_key = (regex.pattern, index)
    if (_cache_key in _cache):
        return _cache[_cache_key]
    result = _get_group_pattern(regex, index)
    _cache[_cache_key] = result
    return result

__all__ = \
[
    'decompile_subpattern',
    'get_group_pattern',
]
