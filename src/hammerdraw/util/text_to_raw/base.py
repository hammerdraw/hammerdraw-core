import re
from logging import Logger
from typing import *

from typing.io import *
from typing.re import *

from hammerdraw.compilers import CompilerBase
from hammerdraw.text_drawer.text_funcs import decapitalize_smart
from .errors import *
from .regex_analyzer import get_group_pattern

def parse_text_file \
(
    compiler: Type[CompilerBase],
    file: TextIO,
    regex: Pattern,
    *,
    logger: Logger = None,
    multiline_fields: List[str] = None,
    decapitalize_fields: List[str] = None,
    numeric_fields: List[str] = None,
    expand_fields: List[str] = None,
    list_fields: Dict[str, str] = None,
    structure_fields: Dict[str, Tuple[str, List[str]]] = None,
    update_fields: Dict[str, Callable[[Any], Any]] = None,
    add_fields: Dict[str, Any] = None,
    separator: str = '=' * 29,
    **kwargs,
) -> int:
    if (logger is None):
        from scripts.common import setup
        logger = setup()
    
    errors = 0
    for data in file.read().split(separator)[1:]:
        data = data.strip()
        
        if (not data):
            continue
        if ('#ignore' in data.splitlines()):
            continue
        
        try:
            result = parse_raw_data \
            (
                compiler,
                data,
                regex = regex,
                logger = logger,
                multiline_fields = multiline_fields,
                decapitalize_fields = decapitalize_fields,
                numeric_fields = numeric_fields,
                expand_fields = expand_fields,
                list_fields = list_fields,
                structure_fields = structure_fields,
                update_fields = update_fields,
                add_fields = add_fields,
                **kwargs,
            )
        except TextToRawException as e:
            logger.error(f"Error: {e}")
            errors += 1
        else:
            filename = result.save_raw()
            logger.info(f"File saved as {filename}")
    
    return errors


def parse_raw_data \
(
    compiler: Type[CompilerBase],
    text_data: str,
    *,
    regex: Pattern,
    logger: Logger,
    multiline_fields: List[str] = None,
    decapitalize_fields: List[str] = None,
    numeric_fields: List[str] = None,
    expand_fields: List[str] = None,
    list_fields: Dict[str, str] = None,
    structure_fields: Dict[str, Tuple[str, List[str]]] = None,
    update_fields: Union[Callable[[CompilerBase], CompilerBase], Dict[str, Callable[[Any], Any]]] = None,
    add_fields: Dict[str, Any] = None,
    compiler_args: Dict[str, Any] = None,
) -> CompilerBase:
    logger.info(f"Parsing data: {(text_data.splitlines() or [ 'Unknown data' ])[0]}")
    
    m = regex.match(text_data)
    if (not m):
        logger.error("Error: Cannot parse data")
        raise CannotParseTextException()
    
    if (compiler_args is None):
        compiler_args = dict()
    instance = compiler(**compiler_args)
    instance.create()
    if (instance.raw is None):
        raise CannotCreateTemplateException
    instance.raw.update(m.groupdict())
    
    map_fields(instance, instance.raw.keys(), lambda s: s.strip())
    
    for key, sub_key in (list_fields or {}).items():
        pat = get_group_pattern(regex, sub_key)
        map_field(instance, key, lambda s: unwrap_list_field(pat, s))
        map_field(instance, f'{key}.[{sub_key}]', lambda s: s.strip())
        delete_field(instance, sub_key)
    
    for key, (prefix, attributes) in (structure_fields or {}).items():
        pat = get_group_pattern(regex, prefix)
        map_field(instance, key, lambda s: unwrap_structure_field(pat, prefix, s))
        map_field(instance, f'{key}._', lambda s: s.strip())
        delete_fields(instance, (f'{prefix}_{attr}' for attr in attributes))
    
    if (not update_fields):
        pass
    elif (callable(update_fields)):
        instance = update_fields(instance)
    else:
        for key, func in (update_fields or {}).items():
            map_field(instance, key, func, only_str_fields=False)
    
    # noinspection PyProtectedMember, PyUnresolvedReferences
    from .base import add_fields as _add_fields
    _add_fields(instance, (add_fields or {}).keys(), lambda k: add_fields[k])
    map_fields(instance, multiline_fields, lambda s: re.sub(r'([^\n])\n([^\n])', r'\1 \2', s))
    map_fields(instance, decapitalize_fields, lambda s: decapitalize_smart(s))
    map_fields(instance, numeric_fields, lambda s: int(s))
    
    _name = instance.generate_filename()
    map_fields(instance, expand_fields, lambda s: s.format(**instance.raw, filename=_name))
    
    if (not instance.validate_raw(instance.raw)):
        raise InvalidRawException()
    return instance

def unwrap_list_field(group_pattern: Pattern, text: str) -> List[str]:
    return [ m.group(0) for m in group_pattern.finditer(text) ]

def unwrap_structure_field(group_pattern: Pattern, prefix: str, text: str) -> Dict[str, str]:
    m = group_pattern.match(text)
    result = dict()
    for k, v in m.groupdict().items():
        _left, _match, _right = k.partition(prefix + '_')
        if (not _left and _match):
            result[_right] = v
    return result

T = TypeVar('T')
def map_fields(instance: CompilerBase, keys: Optional[Iterable[str]], func: Callable[[str], T], *, only_str_fields: bool = True):
    for key in keys or []:
        map_field(instance, key, func, only_str_fields=only_str_fields)
def map_field(instance: CompilerBase, key: str, func: Callable[[str], T], *, only_str_fields: bool = True):
    for _container, _key in find_objects(instance, key):
        if (not only_str_fields or isinstance(_container[_key], str)):
            _container[_key] = func(_container[_key])

def flat_map_field(instance: CompilerBase, key: str, func: Callable[[str], T], *, only_str_fields: bool = True):
    for _container, _key in find_objects(instance, key):
        if (not only_str_fields or isinstance(_container[_key], str)):
            _container = func(_container[_key])

def add_fields(instance: CompilerBase, keys: Optional[Iterable[str]], func: Callable[[str], T]):
    for key in keys or []:
        add_field(instance, key, func)
def add_field(instance: CompilerBase, key: str, func: Callable[[str], T]):
    for _container, _key in find_objects(instance, key, ignore_missing=True):
        _container[_key] = func(key)

def delete_fields(instance: CompilerBase, keys: Optional[Iterable[str]]):
    for key in keys or []:
        delete_field(instance, key)
def delete_field(instance: CompilerBase, key: str):
    for _container, _key in find_objects(instance, key):
        del _container[_key]

def find_objects(instance: CompilerBase, key: str, *, ignore_missing: bool = False) -> Iterator[Tuple[T, Union[str, int]]]:
    yield from _find_objects(instance.raw, key, ignore_missing=ignore_missing)
def _find_objects(obj: Union[Dict[str, T], List[T], T], key: str, *, ignore_missing: bool) -> Iterator[Tuple[T, Union[str, int]]]:
    left, sep, right = key.partition('.')
    if (left.startswith('[') and left.endswith(']')):
        if (not isinstance(obj, list)):
            raise TypeError(f"{obj} is not list while expected (key: {key})")
        for i in range(len(obj)):
            if (not right):
                yield obj, i
            else:
                yield from _find_objects(obj[i], right, ignore_missing=ignore_missing)
    elif (not isinstance(obj, dict)):
        raise TypeError(f"{obj} is not dict while expected (key: {key})")
    elif (left == '_'):
        if (not right):
            for k in obj:
                yield obj, k
        else:
            for k in obj:
                yield from _find_objects(obj[k], right, ignore_missing=ignore_missing)
    elif (left not in obj and not ignore_missing):
        raise KeyError(f"{obj} does not contain key {key}")
    elif (not right):
        yield obj, left
    else:
        yield from _find_objects(obj[left], right, ignore_missing=ignore_missing)

__all__ = \
[
    'parse_text_file',
    'parse_raw_data',
]
