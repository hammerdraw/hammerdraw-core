import os
from typing import Union

from .config_loader import ConfigLoader
from .logging_tools import default_logger
from .preloads import setup_logging, preload_fonts

def setup(*, log_level: Union[int, str] = None, main_config: 'HammerDrawConfig' = None, logging_config_path: str = None):
    # Configure logger
    kw = dict()
    if (log_level):
        kw['default_level'] = log_level
    if (logging_config_path):
        kw['logging_config'] = logging_config_path
    elif (main_config):
        kw['logging_config'] = main_config.with_root().logging_config
    
    if (not os.path.isdir('logs')):
        os.mkdir('logs')
    
    setup_logging(**kw)
    preload_fonts(main_config is not None and main_config.text_drawer or None)
    logger = default_logger
    logger.info("Logger started")
    ConfigLoader.load_configs(main_config=main_config)
    
    return logger

__all__ = \
[
    'setup',
]
