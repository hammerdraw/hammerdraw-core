try:
    import json5 as json
except ImportError:
    import json
else:
    from json import JSONDecodeError as __json_decode_error
    json.JSONDecodeError = __json_decode_error

from ._setup import *
from .color_parser import *
from .config_loader import *
from .dict_operations import *
from .joiner import *
from .logging_tools import *
from .preloads import *
from .ref_resolver import *
from .types import *

__all__ = \
[
    *color_parser.__all__,
    *config_loader.__all__,
    *dict_operations.__all__,
    *joiner.__all__,
    *logging_tools.__all__,
    *preloads.__all__,
    *ref_resolver.__all__,
    *_setup.__all__,
    *types.__all__,
]
