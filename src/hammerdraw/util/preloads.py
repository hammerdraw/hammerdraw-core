import logging
import logging.config
import os
from typing import *

from . import json

logger = logging.getLogger('hammerdraw.util.preloads')
def setup_logging \
(
    logging_config: Union[str, dict] = 'configs/logging.json',
    default_level: Union[str, int] = logging.INFO,
    env_key: str = 'LOG_CFG',
):
    """
    Setup logging configuration
    """
    
    value = os.getenv(env_key, None)
    if (value):
        path = value
    else:
        path = logging_config
    
    config = None
    if (isinstance(path, dict)):
        config = path
    elif (isinstance(path, str) and os.path.isfile(path)):
        with open(path, 'rt') as f:
            config = json.load(f)
    
    if (config is not None):
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

def preload_fonts(text_drawer_config: 'TextDrawerConfig' = None):
    from hammerdraw.text_drawer import TextDrawerConfig
    text_drawer_config: Optional[TextDrawerConfig]
    
    if (text_drawer_config is not None):
        default_finder = text_drawer_config.text_drawer_class.cls.get_default_text_finder()
        default_finder.font_directories = text_drawer_config.font_directories + default_finder.font_directories
        for params in text_drawer_config.font_preloads:
            logger.debug(f"Preloading font: {params}")
            path = default_finder.preload_font(**params)
            if (path):
                logger.info(f"Font preloaded: {params} => {path!r}")
            else:
                logger.warning(f"Cannot preload font: {params}")
        
        return

__all__ = \
[
    'setup_logging',
    'preload_fonts',
]
