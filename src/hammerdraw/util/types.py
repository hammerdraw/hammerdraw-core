from typing import *

from PIL.Image import Image

Point = Tuple[int, int]
""" A type alias representing a point on the flat """
Size = Tuple[int, int]
""" A type alias representing an area size """
Region = Tuple[int, int, int, int]
""" A type alias representing a region with by point and size """

GetterType = Callable[[Any], Any]
""" A type alias representing any getter """
SetterType = Callable[[Any, Any], Any]
""" A type alias representing any setter """

RawType = Dict[str, Any]
""" A type alias representing a source data type """

ImageType = Image

__all__ = \
[
    'GetterType',
    'ImageType',
    'Point',
    'RawType',
    'Region',
    'SetterType',
    'Size',
]
