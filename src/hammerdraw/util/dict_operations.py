from copy import copy
from typing import *

T1 = TypeVar('T1')
T2 = TypeVar('T2')
def dict_subtract(x: Dict[T1, T2], *keys: T1) -> Dict[T1, T2]:
    result = dict()
    for key in keys:
        if key in x:
            result[key] = x.pop(key)
    
    return result
del T1, T2

D = TypeVar('D', bound=Mapping)
def dict_filter_out(d: D, filter_func: Callable[[Any, Any], bool] = lambda k, v: v is None) -> D:
    d = copy(d)
    for k, v in list(d.items()):
        if (filter_func(k, v)):
            del d[k]
    return d
del D

__all__ = \
[
    'dict_subtract',
    'dict_filter_out',
]
