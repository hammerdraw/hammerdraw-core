import datetime
from functools import wraps
from logging import Logger, DEBUG, getLogger, _checkLevel
from typing import *

default_logger = getLogger('hammerdraw')

def log_time(func_name: str = None, logger: Logger = None, log_level: Union[str, int] = DEBUG):
    _func = None
    if (callable(func_name)):
        _func, func_name = func_name, _func
    if (logger is None):
        logger = default_logger
    if (not isinstance(log_level, int)):
        log_level = _checkLevel(log_level)
    
    def wrapper(func: Callable):
        _func_name = func_name if (func_name is not None) else func.__name__
        
        @wraps(func)
        def wrapped(*args, **kwargs):
            _now = datetime.datetime.now()
            logger.log(log_level, "Calling %s", _func_name)
            result = func(*args, **kwargs)
            logger.log(log_level, "%s exit. Time spent: %ims", func_name, (datetime.datetime.now() - _now).total_seconds() * 1000)
            return result
        return wrapped
    
    if (_func is not None):
        return wrapper(_func)
    else:
        return wrapper

__all__ = \
[
    'default_logger',
    'log_time',
]
