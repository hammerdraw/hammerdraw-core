import glob
import os.path
import re
from abc import ABC
from typing import *
from typing import Any

import jsonschema.exceptions
from PIL import Image
from jsonschema import validate
from yn_input import yn_input

from dataclasses_config import Path
from functional import Option, Some
from hammerdraw.config import HammerDrawConfig
from hammerdraw.interfaces import IConfigurable, ICompiler
from hammerdraw.text_drawer import TextDrawer
from hammerdraw.util import ConfigLoader
from hammerdraw.util import ExtendedRefResolver
from hammerdraw.util import RawType, ImageType
from hammerdraw.util import json
from .image_operator import ImageOperator
from .module_compiler import ModuleCompiler

class CompilerBase(ImageOperator, ModuleCompiler, ICompiler, IConfigurable[HammerDrawConfig], ABC):
    module_name: str = None
    base: ImageType = None
    compiled_filename: Optional[Path] = None
    filename: Optional[Path] = None
    compiled: Optional[ImageType] = None
    raw: Optional[Dict[str, Any]] = None
    
    def __init__(self, config: Optional[HammerDrawConfig] = None, *args, **kwargs):
        self.logger_name = getattr(self, 'logger_name', None) or f"hammerdraw.compilers.{self.module_name}.{self.compiler_type}-compiler"
        config = (Option(config) or Some(HammerDrawConfig.default())).get.with_root(compiler=self)
        config = config.replace(compilers=config.compilers.with_root(default_path=config.module_directory, compiler=self))
        super().__init__(config, *args, **kwargs)
    
    @property
    def module_root(self) -> Path:
        return self.config.compilers.root
    @property
    def raw_directory_root(self) -> Path:
        return self.config.raw_directory_root.as_path
    @property
    def raw_directory(self) -> Path:
        return Path(self.raw_directory_root, self.get_from_config('rawDirectory'))
    @property
    def sources_directory(self) -> Path:
        return self.config.compilers.sources_directory.as_path
    @property
    def schema_path(self) -> Path:
        return self.config.compilers.schema_path.as_path
    @property
    def template_path(self) -> Path:
        return self.config.compilers.template_path.as_path
    @property
    def output_directory(self) -> Path:
        return Path(self.config.output_directory_root, self.get_from_config('outputDirectory'))
    @property
    def default_extension_priorities(self) -> Iterable[str]:
        return [ 'json' ]
    @property
    def text_drawer_class(self) -> Type[TextDrawer]:
        return self.config.text_drawer.text_drawer_class.cls
    
    def search(self, *args, **kwargs) -> List[str]:
        formatted = [ filename.replace('\\', '/') for filename in sorted(list(self.isearch(*args, **kwargs))) ]
        return formatted
    
    def isearch(self, ignore_dummies: bool = True, extension: Union[str, Iterable[str]] = None, directory: str = None) -> Iterator[Path]:
        if (extension is None):
            extension = self.default_extension_priorities
        if (isinstance(extension, str)):
            extension = [ extension ]
        
        if (directory is None):
            directory = self.raw_directory
        
        for ext in extension:
            yield from self._isearch(ignore_dummies=ignore_dummies, extension=ext, directory=directory)
    
    def _isearch(self, ignore_dummies: bool, extension: str, directory: str) -> Iterator[Path]:
        for entry in glob.iglob(Path(directory, f'**/*.{extension}'), recursive=True):
            if (ignore_dummies and entry.endswith(f'.dummy.{extension}')):
                continue
            yield Path(entry)
    
    def find(self, name: str, **kwargs) -> Optional[Path]:
        filename = Path(name)
        if (os.path.isfile(filename)):
            return filename
        
        for ext in self.default_extension_priorities:
            filename = Path(self.raw_directory, f'{name}.{ext}')
            if (os.path.isfile(filename)):
                return filename
        
        for filename in self.isearch(**kwargs):
            try:
                with open(filename) as _file:
                    _json = json.load(_file)
                
                _name = _json.get('name', None)
                if (_name and isinstance(_name, str) and name.lower() in _name.lower()):
                    message = "{type} found - {filename}:\n  {name}{subtitle}".format \
                    (
                        type = self.compiler_type.capitalize(),
                        filename = filename,
                        name = _name,
                        subtitle = " ({subtitle})".format_map(_json) if (_json.get('subtitle', None)) else '',
                    )
                    print(message)
                    if (yn_input('Is it what you need?')):
                        return filename
            
            except (OSError, FileNotFoundError, json.JSONDecodeError) as e:
                self.logger.warning(f"Cannot open file {filename}: {e}")
        
        return None
    
    def create(self):
        self.open(self.template_path)
        self.filename = None
    
    def validate_raw(self, raw):
        _schema_filename = self.schema_path
        self.logger.debug(f"Reading '{_schema_filename}'...")
        with open(_schema_filename) as file:
            schema = json.load(file)
        
        try:
            self.logger.debug(f"Validating '{ raw.get('name', '== Unnamed ==') }'...")
            validate(raw, schema, resolver=ExtendedRefResolver(base_uri='file://' + os.path.dirname(os.path.abspath(self.schema_path)) + '/', referrer=schema))
        except jsonschema.exceptions.ValidationError as e:
            self.logger.error(f"Opened raw is not valid: {e.message}")
            return False
        else:
            self.logger.debug("Raw file is valid")
            return True
    
    # noinspection PyMethodOverriding
    def open(self, name, *, ignore_schema: bool = False, **kwargs) -> Optional[RawType]:
        _filename = self.find(name, **kwargs)
        self.compiled = None
        if (not _filename):
            self.logger.error(f"Cannot find {self.compiler_type}: '{name}'")
            return None
        
        self.logger.info(f"Reading '{_filename}'...")
        with open(_filename, 'rb') as file:
            raw = json.load(file)
        
        self.raw = None
        if (not ignore_schema):
            if (self.validate_raw(raw)):
                self.raw = raw
        else:
            self.logger.debug("Raw file checks were ignored")
            self.raw = raw
        
        self.filename = _filename
        self.compiled_filename = None
        return self.raw
    
    def _get_base_filename(self) -> str:
        return self.get_from_config('baseNameTemplate')
    
    def prepare_base(self) -> Optional[ImageType]:
        name = self._get_base_filename()
        
        filepath = Path(self.sources_directory, name)
        if (not os.path.isfile(filepath)):
            self.logger.error(f"Cannot load {self.compiler_type} template: '{filepath}'")
            return None
        
        base = Image.open(filepath)
        self.logger.info("Base prepared")
        return base
    
    def compile(self) -> Optional[ImageType]:
        self.compiled = None
        self.init_modules()
        
        self.base = self.prepare_base()
        if (not self.base):
            return None
        _base = self.base.copy()
        
        if not (self.compile_modules(_base)):
            return None
        
        if not (self.join(_base)):
            return None
        
        self.compiled = _base
        self.logger.info("{type} compiled!".format(type=self.compiler_type.capitalize()))
        return self.compiled
    
    def update(self) -> Optional[ImageType]:
        if (not self.base):
            return None
        _base = self.base.copy()
        
        if not (self.join(_base)):
            return None
        
        self.compiled = _base
        self.logger.info("{type} updated!".format(type=self.compiler_type.capitalize()))
        return self.compiled
    
    def generate_filename(self) -> str:
        return self._generate_filename(self.get_from_raw('name'))
    def _generate_filename(self, name: str):
        name = name.lower()
        name = name.replace(' ', '-')
        
        name = re.sub(r'[:"*?<>|]+', '', name)
        name = re.sub(r'([.\-])\1+', r'\1', name)
        name = re.sub(r'^([.\-])|([.\-])$', '', name)
        
        return name
    
    def save_raw(self) -> Optional[str]:
        if (not self.validate_raw(self.raw)):
            return None
        
        _filename = self.filename
        if not (_filename):
            _name = self.generate_filename()
            _filename = f"{self.raw_directory}{_name}.json"
        
        _filename = re.sub(r'[:"*?<>|]+', '', _filename)
        _filename = os.path.abspath(_filename)
        _dir = os.path.dirname(_filename)
        if (not os.path.exists(_dir)):
            os.makedirs(_dir, exist_ok=True)
        self.logger.debug("Saving raw into '{filename}'...".format(filename=_filename))
        with open(_filename, 'w') as _file:
            json.dump(self.raw, _file, indent=4, sort_keys=True)
        self.filename = _filename
        return _filename
    
    def get_output_name(self) -> Optional[str]:
        return self.output_name or self._get_output_name()
    def _get_output_name(self) -> Optional[str]:
        return self.raw.get('name', None)
    def get_output_directory(self) -> str:
        return self.output_directory.format_map(self.raw)
    
    def save_compiled(self, forced_width: Optional[int] = None, directory: Optional[str] = None) -> Optional[str]:
        if (not self.compiled):
            self.logger.error("Could not save not compiled result")
            return None
        
        name = self.get_output_name()
        if (not name):
            self.logger.error("Could not find proper name")
            return None
        
        _image = self.compiled
        if (forced_width):
            _estimated_width = forced_width
            
            _actual_width = _image.width
            _actual_height = _image.height
            _image = _image.resize((int(_estimated_width), int(_estimated_width / _actual_width * _actual_height)), Image.ANTIALIAS)
        
        _path = (Option.from_optional(self.compiled_filename) or (Option.from_optional(directory) or Some(self.get_output_directory())).map(lambda d: Path(d, f'{name}.png'))).get
        _dirname = os.path.dirname(_path)
        _file = os.path.basename(_path)
        _file = re.sub(r'[:"*?<>|]+', '', _file)
        
        _filename = Path(_dirname, _file)
        if (not os.path.isdir(_dirname)):
            try:
                os.makedirs(_dirname)
            except OSError as exc:  # Guard against race condition
                import errno
                if exc.errno != errno.EEXIST:
                    raise
        try:
            self.logger.info("Saving compiled file: '{filename}'".format(filename=_filename))
            _image.save(_filename)
        except:
            self.logger.exception("Error while saving file '{filename}'".format(filename=_filename))
            return None
        else:
            self.compiled_filename = _filename
            return _filename
    
    def get_from_config(self, path: str, default = None) -> Any:
        return ConfigLoader.get_from_config(path, config_name=f'{self.module_name}:{self.compiler_type}', default=default)

__all__ = \
[
    'CompilerBase',
]
