from abc import ABC
from typing import *

import dpath.util

from hammerdraw.interfaces import ILoggable, ICompilable, IModule, ModuleDeclaration, ModulesIndexType, IImageOperator
from hammerdraw.text_drawer import TextDrawer
from hammerdraw.util import ImageType, RawType
from .compiler_error import CompilerError

_DEFAULT_VALUE__MC = object()
class ModuleCompiler(ICompilable, IImageOperator, ILoggable, ABC):
    raw: RawType = None
    modules: List[ModuleDeclaration] = None
    
    module_root: str
    raw_directory_root: str
    raw_directory: str
    sources_directory: str
    
    compiled: ImageType = None
    compiled_modules: Dict[ModulesIndexType, ImageType] = None
    initialized_modules: Dict[ModulesIndexType, ICompilable] = None
    continuous_print: Dict[str, Union[int, str]] = None
    
    @property
    def text_drawer_class(self) -> Type[TextDrawer]:
        raise NotImplementedError
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.continuous_print = dict()
    
    def init_modules(self) -> bool:
        self.initialized_modules = dict()
        for i, _ in enumerate(self.modules):
            self.init_module(i)
        
        return True
    
    def init_module(self, index: ModulesIndexType) -> ModuleDeclaration:
        module = self.modules[index]
        
        _module_type = None
        _module_args = None
        _module_kwargs = None
        if (isinstance(module, type)):
            _module_type = module
        elif (isinstance(module, tuple)):
            _module_type = module[0]
            if (len(module) > 1):
                if (len(module) == 2 and isinstance(module[1], dict)):
                    _module_kwargs = module[1]
                elif (len(module) == 2 and isinstance(module[1], (list, tuple))):
                    _module_args = module[1]
                else:
                    _module_args = module[1:]
        else:
            raise TypeError("Module should be either type or tuple")
        
        _args = _module_args or list()
        _kwargs = _module_kwargs or dict()
        module_object = _module_type(self, index, *_args, **_kwargs)
        self.initialized_modules[index] = module_object
        return module_object
    
    def compile_modules(self, base: ImageType, raise_errors: bool = False) -> bool:
        self.compiled_modules = dict()
        
        for i, _module in enumerate(self.modules):
            if (not self.compile_module(i, raise_errors=raise_errors)):
                return False
        
        return True
    
    def compile_module(self, index: ModulesIndexType, *, raise_errors: bool = False) -> Union[bool, IModule]:
        if (raise_errors):
            return self._compile_module(index)
        
        try:
            module_object = self._compile_module(index)
        except (FileNotFoundError, CompilerError) as e:
            self.logger.error(f"Error while compiling the module #{index}: {self.modules[index]}")
            self.logger.exception(str(e))
            return False
        except Exception as e:
            self.logger.error(f"{e}")
            self.logger.exception(f"Critical error while compiling the module #{index}: {self.modules[index]}")
            return False
        else:
            return module_object
    
    def _compile_module(self, index: ModulesIndexType):
        module_object: IModule = self.initialized_modules.get(index)
        self.compiled_modules[index] = module_object.compile()
        return module_object
    
    def join(self, base: ImageType) -> bool:
        for _index, _ in enumerate(self.modules):
            _module: IModule = self.initialized_modules.get(_index)
            _module.insert(base)
        return True
    
    T = TypeVar('T')
    def get_from_raw(self, key: str, default: Optional[T] = _DEFAULT_VALUE__MC) -> T:
        try:
            # return self.raw.get(key)
            return dpath.util.get(self.raw, key)
        except KeyError:
            if (default is not _DEFAULT_VALUE__MC):
                return default
            else:
                raise

__all__ = \
[
    'ModuleCompiler',
]
