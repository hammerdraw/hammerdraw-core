from abc import ABC
from typing import List, Union, Callable, Optional

from PIL import Image

from hammerdraw.interfaces import ILoggable, IImageOperator, GradientPart
from hammerdraw.text_drawer import TextDrawer, CapitalizationModes
from hammerdraw.util import Point, Region, ImageType, RawType, get_color
from .compiler_error import InvalidConfigError

class ImageOperator(IImageOperator, ILoggable, ABC):
    def insert_table \
    (
        self,
        vertical_columns: List[int],
        top: int,
        cell_height: int,
        data: RawType,
        *,
        
        body_row_template: List[str],
        body_text_drawer: TextDrawer,
        body_row_interval: int,
        body_capitalization: Union[CapitalizationModes, str, None] = None,
        body_bold: Optional[bool] = None,
        body_italic: Optional[bool] = None,
        
        header_row: List[str] = None,
        header_text_drawer: TextDrawer = None,
        header_row_interval: int = None,
        header_capitalization: Union[CapitalizationModes, str, None] = None,
        header_bold: Optional[bool] = None,
        header_italic: Optional[bool] = None,
    ):
        if (top < 0):
            direction = -1
            indirect = True
        else:
            direction = 1
            indirect = False
        
        total_height = 0
        y1 = top
        y2 = cell_height and (top + cell_height)
        
        if (header_row and not indirect):
            y1, y2, total_height = self._insert_table__print_header_row \
            (
                vertical_columns = vertical_columns,
                y1 = y1,
                y2 = y2,
                total_height = total_height,
                
                body_row_template = body_row_template,
                body_text_drawer = body_text_drawer,
                body_row_interval = body_row_interval,
                body_capitalization = body_capitalization,
                body_bold = body_bold,
                body_italic = body_italic,
                
                header_row = header_row,
                header_text_drawer = header_text_drawer,
                header_row_interval = header_row_interval,
                header_capitalization = header_capitalization,
                header_bold = header_bold,
                header_italic = header_italic,
            )
        
        text_drawer = body_text_drawer
        _font = text_drawer.get_font()
        text_drawer.set_font(capitalization=body_capitalization, bold=body_bold, italic=body_italic)
        
        dy = body_row_interval
        _data = reversed(data) if (indirect) else data
        for data_row in _data:
            table_row = body_row_template
            _h = self._insert_table__print_table_row(y1=y1, y2=y2, text_drawer=text_drawer, vertical_columns=vertical_columns, table_row=table_row, data_row=data_row)
            y1 += _h + dy
            total_height += _h + dy
            if (y2):
                y2 += _h + dy
        
        self.logger.debug("Restoring font {font}".format(font=_font))
        text_drawer.set_font(**_font)
        
        if (header_row and indirect):
            y1, y2, total_height = self._insert_table__print_header_row \
            (
                vertical_columns = vertical_columns,
                y1 = y1,
                y2 = y2,
                total_height = total_height,
                
                body_row_template = body_row_template,
                body_text_drawer = body_text_drawer,
                body_row_interval = body_row_interval,
                body_capitalization = body_capitalization,
                body_bold = body_bold,
                body_italic = body_italic,
                
                header_row = header_row,
                header_text_drawer = header_text_drawer,
                header_row_interval = header_row_interval,
                header_capitalization = header_capitalization,
                header_bold = header_bold,
                header_italic = header_italic,
            )
        
        return total_height
    
    def _insert_table__print_header_row \
    (
        self, vertical_columns, y1, y2, total_height,
        body_row_template, body_text_drawer, body_row_interval, body_capitalization=None, body_bold=None, body_italic=None,
        header_row=None, header_text_drawer=None, header_row_interval=None, header_capitalization=None,header_bold=None, header_italic=None,
    ):
        text_drawer = header_text_drawer or body_text_drawer
        
        _font = text_drawer.get_font()
        text_drawer.set_font(capitalization=header_capitalization, bold=header_bold, italic=header_italic)
        
        dy = header_row_interval or body_row_interval
        data_row = { }
        table_row = header_row
        _h = self._insert_table__print_table_row(y1=y1, y2=y2, text_drawer=text_drawer, vertical_columns=vertical_columns, table_row=table_row, data_row=data_row)
        y1 += _h + dy
        total_height += _h + dy
        if (y2):
            y2 += _h + dy
        
        self.logger.debug(f"Restoring font {_font}")
        text_drawer.set_font(**_font)
        return y1, y2, total_height
    
    def _insert_table__print_table_row(self, y1, y2, text_drawer, vertical_columns, table_row, data_row):
        max_h = 0
        for i, _cell in enumerate(table_row):
            x1 = vertical_columns[i]
            x2 = vertical_columns[i + 1]
            _, _h = text_drawer.get_text_size((x1, y1, x2, y2), _cell.format_map(data_row), offset_borders=False)
            if (max_h < _h):
                max_h = _h
        
        _y1 = -y1 - max_h if (y1 < 0) else y1
        _y2 = (-y2 - max_h if (y2 < 0) else y2) if y2 else _y1 + max_h
        
        for i, _cell in enumerate(table_row):
            x1 = vertical_columns[i]
            x2 = vertical_columns[i + 1]
            
            text_drawer.print_in_region((x1, _y1, x2, _y2), _cell.format_map(data_row), offset_borders=False)
        
        return max_h
    
    def insert_image_scaled \
    (
        self,
        base_image: ImageType,
        region: Region,
        image_path: Union[str, ImageType],
        *,
        offset_borders: bool = True,
        scale_func: Callable[[float, float], float] = max,
        foreground: bool = False,
        center: bool = False,
    ):
        if (offset_borders):
            x, y, w, h = region
        else:
            x, y, x2, y2 = region
            w = x2 - x
            h = y2 - y
        
        if (isinstance(image_path, ImageType)):
            self.logger.debug(f"Inserting image to position {region} with scaling and reversed mask")
            image = image_path
        else:
            self.logger.debug(f"Inserting image '{image_path}' to position {region} with scaling and reversed mask")
            image = Image.open(image_path)
        
        _estimated_width = w
        _estimated_height = h
        
        _actual_width = image.width
        _actual_height = image.height
        
        _width_scale = _estimated_width / _actual_width
        _height_scale = _estimated_height / _actual_height
        
        _new_scale = scale_func(_width_scale, _height_scale)
        _image = image.resize((int(_new_scale * _actual_width), int(_new_scale * _actual_height)), Image.ANTIALIAS)
        _image = _image.convert('RGBA')
        
        if (center):
            x += (w - _image.width) // 2
            y += (h - _image.height) // 2
        
        _im_copy = None
        if (not foreground):
            _im_copy = base_image.copy()
        base_image.paste(_image, (x, y), mask=_image)
        if (not foreground):
            base_image.paste(_im_copy, (0, 0), _im_copy)
        return base_image
    
    def get_image_size(self, image_path: str) -> Point:
        image = Image.open(image_path)
        _w = image.width
        _h = image.height
        return _w, _h
    
    def insert_image_centered(self, base_image: ImageType, position: Point, image_path: str, *, offset_borders: bool = True, scale: int = 1.0) -> Region:
        x, y = position
        
        self.logger.debug(f"Inserting image '{image_path}' to position {position}")
        image: ImageType = Image.open(image_path)
        image = image.resize((int(scale * image.width), int(scale * image.height)), Image.ANTIALIAS)
        
        _w = image.width
        _h = image.height
        
        _x = x - _w // 2; _y = y - _h // 2
        _image = image
        if (_image.mode.endswith('A')):
            _image = image.convert(image.mode[:-1])
        base_image.paste(_image, (_x, _y), image)
        return _x, _y, _w, _h
    
    def get_gradient_base(self, width: int, gradient: List[GradientPart]) -> ImageType:
        if (not isinstance(gradient, list)):
            raise InvalidConfigError("Gradient section should be a list.")
        if (len(gradient) < 2):
            raise InvalidConfigError("Gradient section should have at least 2 points.")
        
        im = Image.new("RGBA", size=(width, 1))
        
        for i in range(len(gradient) - 1):
            x1 = int(gradient[i].position * width)
            x2 = int(gradient[i + 1].position * width)
            
            _color1 = get_color(gradient[i].color)
            _color2 = get_color(gradient[i + 1].color)
            
            r1, g1, b1, *_a1 = _color1
            r2, g2, b2, *_a2 = _color2
            if (_a1 and _a2):
                # Extract alpha
                a1, = _a1
                a2, = _a2
            else:
                # No alpha
                a1 = 0xff
                a2 = 0xff
            
            for dx in range(x1, x2):
                offset = (dx - x1) / (x2 - x1)
                r = int(r1 + (r2 - r1) * offset)
                g = int(g1 + (g2 - g1) * offset)
                b = int(b1 + (b2 - b1) * offset)
                a = int(a1 + (a2 - a1) * offset)
                im.putpixel((dx, 0), (r, g, b, a))
        
        return im

__all__ = \
[
    'ImageOperator',
]
