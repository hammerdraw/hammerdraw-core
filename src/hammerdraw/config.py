from typing import *

from dataclasses import dataclass
from dataclasses_json import config as dcj_config, LetterCase

from dataclasses_config import *
from .interfaces import ICompiler
from .text_drawer import TextDrawerConfig

@dataclass(frozen=Settings.frozen)
class _CompilerConfig(Config):
    schema_path: RelPath
    template_path: RelPath
    sources_directory: RelPath

@dataclass(frozen=Settings.frozen)
class CompilerConfig(ConfigWithRoot, _CompilerConfig):
    root: Optional[Path] = None
    
    # noinspection PyTypeChecker
    dataclass_json_config = dcj_config(letter_case=LetterCase.CAMEL)['dataclasses_json']

@main_config(default_config_name='hammerdraw.conf', resources_directory='configs/', root_config_path='hammerdraw', log_config=False)
class HammerDrawConfig(MainConfig, ConfigWithRoot):
    compilers: CompilerConfig
    text_drawer: TextDrawerConfig
    
    logging_config: RelPath
    module_directory: RelPath
    output_directory_root: RelPath
    raw_directory_root: RelPath
    
    # noinspection PyTypeChecker
    dataclass_json_config = dcj_config(letter_case=LetterCase.CAMEL)['dataclasses_json']

__all__ = \
[
    'CompilerConfig',
    'HammerDrawConfig',
]


def main():
    # noinspection PyAbstractClass
    class SomeCompiler(ICompiler):
        module_name: str = "test_module"
        compiler_type = "some_compiler"
    
    compiler = SomeCompiler()
    
    config = HammerDrawConfig.default().with_root(compiler=compiler)
    print(config)
    
    config.compilers = config.compilers.with_root(default_path=config.module_directory, compiler=compiler)
    print(config.compilers)
    print(config.compilers.schema_path)
    
    return 0

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
