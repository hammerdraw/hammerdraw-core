from typing import *

from dataclasses_config import Path
from hammerdraw.compiler_modules import ModuleBase
import os

class ImageModule(ModuleBase):
    module_name: str = None
    raw_field: str = None
    image_directory : str = None
    image_path: str = None
    path_from_config: bool
    format_path: bool
    insert_img_kwargs: Dict[str, Any]
    
    def __init__(self, *args, name: str = 'image', **kwargs):
        self.module_name = name
        super().__init__(*args, **kwargs)
    
    def initialize(self, field: str = None, image_directory: str = None, path_from_config: bool = False, format_path: bool = False, **kwargs):
        self.insert_img_kwargs = dict()
        for arg_name in [ 'scale_func', 'foreground', 'offset_borders', 'center' ]:
            if (arg_name in kwargs):
                self.insert_img_kwargs.setdefault(arg_name, kwargs.pop(arg_name))
        
        self.raw_field = field or self.module_name
        self.image_directory = image_directory or self.parent.raw_directory_root
        self.path_from_config = path_from_config
        self.format_path = format_path
        super().initialize(**kwargs)
    
    def _compile(self, base):
        if (not self.parent.get_from_raw(self.raw_field)):
            self.image_path = None
            return 0
        
        filename = self.parent.get_from_raw(self.raw_field)
        if (not os.path.isfile(filename)):
            
            if (self.path_from_config):
                filename = Path(self.parent.sources_directory, self.get_from_config('imagePath'))
            else:
                filename = Path(self.image_directory, self.parent.raw[self.raw_field])
            
            if (self.format_path):
                filename = filename.format_map(self.parent.raw)
            if (not os.path.isfile(filename)):
                self.logger.error(f"Cannot load {self.parent.compiler_type} image by path: '{self.parent.get_from_raw(self.raw_field)}' - no such file")
                self.image_path = None
                return 0
        
        self.image_path = filename
        self.logger.info("Image verified")
        return 0
    
    def insert(self, parent_base):
        if (not self.image_path):
            return
        
        _x, _y = self.get_position()
        self.parent.insert_image_scaled(base_image=parent_base, region=(_x, _y, self.width, self.height), image_path=self.image_path, **self.insert_img_kwargs)
        self.logger.info("Image inserted")

__all__ = \
[
    'ImageModule',
]
