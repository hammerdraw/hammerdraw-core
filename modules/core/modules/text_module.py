from typing import *
from dataclasses import dataclass

from hammerdraw.compiler_modules import ModuleBase, module_attribute
from hammerdraw.compilers.compiler_error import TextNotFitInAreaWarning
from hammerdraw.util import ImageType

@dataclass
class ReduceFontSize(Exception):
    current_scale: float
    new_scale: float

class TextModule(ModuleBase):
    module_name = None
    raw_field: str
    raw_font_size_scale_field = None
    multiline = None
    converter_func: Optional[Callable[[Any], str]] = None
    
    def __init__(self, *args, name: str = None, **kwargs):
        self.module_name = name or self.module_name
        if (self.module_name is None):
            raise ValueError("Module name is missing!")
        super().__init__(*args, **kwargs)
    
    def initialize(self, field:str=None, scale_field:str=None, multiline:bool=False, convert_to_str: Union[bool, Callable[[Any], str], None] = None, **kwargs):
        self.raw_field = field or self.module_name
        self.multiline = multiline
        
        if (convert_to_str == True):
            self.converter_func = '{}'.format
        elif (convert_to_str and callable(convert_to_str)):
            self.converter_func = convert_to_str
        else:
            self.converter_func = None
        
        if (scale_field):
            self.raw_font_size_scale_field = scale_field
        
        super().initialize(**kwargs)
    
    def _get_text(self) -> str:
        text = self.parent.get_from_raw(self.raw_field)
        if (self.converter_func is not None):
            text = self.converter_func(text)
        return text
    
    def _compile(self, base: ImageType):
        text = self._get_text()
        if (text):
            scale = None
            auto_adjust_scale = None
            while True:
                temp_base = base.copy()
                try:
                    result = self._print(temp_base, text, scale=scale, auto_adjust_scale=auto_adjust_scale)
                except ReduceFontSize as e:
                    scale = e.new_scale
                    auto_adjust_scale = True
                else:
                    base.paste(temp_base)
                    return result
        
        return 0
    def _print(self, base: ImageType, text: str, *, scale: float = None, auto_adjust_scale = None):
        td = self.get_text_drawer(base)
        
        if (self.raw_font_size_scale_field):
            if (scale is None):
                scale = self.parent.get_from_raw(self.raw_font_size_scale_field, None)
            
            if (scale is None):
                auto_adjust_scale = self.parent.get_from_raw('autoAdjustScale', False)
                scale = 1.0
            
            if (scale != 1.0):
                td_font = td.get_font()
                td.set_font(font_size=td_font['font_size'] * scale)
        
        w, h = td.print_in_region((0, 0, self.width, self.height), text, offset_borders=True)
        self.logger.info(f"{self.module_name.title()} printed")
        
        if (w > self.width or h > self.height):
            if (auto_adjust_scale):
                _scale_step: Optional[float] = self.get_from_config('autoAdjustStep', lambda: self.parent.get_from_config('autoAdjustStep', 0.05))
                self.logger.error(f"Reducing font size from {scale} to {scale - _scale_step}")
                if (scale > _scale_step):
                    raise ReduceFontSize(scale, scale - _scale_step)
            
            self.logger.error(f"Current scale: {scale}. Auto scale adjustment: {auto_adjust_scale}")
            self.set_error_state(True)
            raise TextNotFitInAreaWarning \
            (
                module = self,
                field = self.raw_field,
                required_size = (self.width, self.height),
                actual_size = (w, h),
                comment = "Please, reduce either text length or font size."
            )
        elif (self._error_state):
            self.set_error_state(False)
        
        return h
    
    _error_state: bool = False
    def set_error_state(self, has_error: bool):
        self._error_state = has_error

__all__ = \
[
    'TextModule',
]
