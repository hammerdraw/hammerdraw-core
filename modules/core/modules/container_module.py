from typing import *

from hammerdraw.compiler_modules import ModuleBase
from hammerdraw.compilers import ModuleCompiler
from hammerdraw.interfaces import ModuleDeclaration, GradientPart, ModulesIndexType
from hammerdraw.util import ImageType, Point, Region

_DEFAULT_VALUE__CM = object()
class ContainerModule(ModuleBase, ModuleCompiler):
    module_name = 'container'
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.init_modules()
    
    def insert_table(self, *args, **kwargs):
        return self.parent.insert_table(*args, **kwargs)
    def insert_image_scaled(self, *args, **kwargs):
        return self.parent.insert_image_scaled(*args, **kwargs)
    def get_image_size(self, *args, **kwargs) -> Point:
        return self.parent.get_image_size(*args, **kwargs)
    def insert_image_centered(self, *args, **kwargs) -> Region:
        return self.parent.insert_image_centered(*args, **kwargs)
    def get_gradient_base(self, *args, **kwargs) -> ImageType:
        return self.parent.get_gradient_base(*args, **kwargs)
    
    # noinspection PyMethodOverriding
    def initialize(self, modules: List[ModuleDeclaration], *args, **kwargs):
        self.modules = modules
        super().initialize(*args, **kwargs)
    
    def _compile(self, base: ImageType):
        self.compile_modules(base, raise_errors=True)
        self.join(base)
        
        return self.height
    
    def update(self):
        for m in self.initialized_modules.values():
            m.update()
    
    def join(self, base: ImageType, *args, **kwargs):
        for _index, _ in enumerate(self.modules):
            _module: ModuleBase = self.initialized_modules.get(_index)
            _module.compiled.save(open(f".temp/{self.module_name}-module-{_index}_{_module.module_name}.bmp", 'wb'))
        v = super().join(base, *args, **kwargs)
        base.save(open(f".temp/{self.module_name}-module-joined.bmp", 'wb'))
        return v
    
    def get_from_raw(self, key: str, default = _DEFAULT_VALUE__CM):
        test = super().get_from_raw(f'{self.module_name}/{key}', default=_DEFAULT_VALUE__CM)
        if (test is not _DEFAULT_VALUE__CM):
            return test
        elif (default is not _DEFAULT_VALUE__CM):
            return super().get_from_raw(key, default=default)
        else:
            return super().get_from_raw(key)
    
    def get_from_config(self, key, default=_DEFAULT_VALUE__CM):
        test = super().get_from_config(key, default=_DEFAULT_VALUE__CM)
        if (test is not _DEFAULT_VALUE__CM):
            return test
        elif (default is not _DEFAULT_VALUE__CM):
            return self.parent.get_from_config(key, default=default)
        else:
            return self.parent.get_from_config(key)
    
    @property
    def raw(self) -> Dict[str, Any]:
        return self.parent.get_from_raw(self.module_name)
    
    @property
    def module_root(self) -> str:
        return self.parent.module_root
    
    @property
    def raw_directory_root(self) -> str:
        return self.parent.raw_directory_root
    
    @property
    def raw_directory(self) -> str:
        return self.parent.raw_directory
    
    @property
    def sources_directory(self) -> str:
        return self.parent.sources_directory
    
    @property
    def compiler_type(self) -> str:
        return self.module_name

__all__ = \
[
    'ContainerModule',
]
