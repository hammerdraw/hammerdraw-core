from hammerdraw.compilers import CompilerBase

class BaseCompiler(CompilerBase):
    module_name = 'core'
    compiler_type = "base-compiler"
    
    def open(self, *args, **kwargs):
        self.raw = dict(name=self.compiler_type)
        return self.raw

__all__ = \
[
    'BaseCompiler',
]
