from random import choices, randint
from typing import Dict, List
from unittest import main, TestCase

import hypothesis.strategies as st
from hypothesis import given, settings, Verbosity

from hammerdraw.util import dict_subtract

class DictSubtractTestcase(TestCase):
    
    @settings(verbosity=Verbosity.verbose)
    @given(st.dictionaries(st.characters(), st.integers()))
    def test_something(self, d: Dict[str, int]):
        d2 = d.copy()
        keys: List[str] = choices(list(d2.keys()), k=randint(0, len(d)))
        d3 = dict_subtract(d2, *keys)
        
        d4 = d2.copy()
        d4.update(d3)
        self.assertDictEqual(d, d4)
        for key in keys:
            self.assertIn(key, d)
            self.assertNotIn(key, d2)
            self.assertIn(key, d3)
            self.assertIn(key, d4)
            
            self.assertEqual(d[key], d3[key])
            self.assertEqual(d[key], d4[key])
        
        for key in d:
            if key not in keys:
                self.assertIn(key, d2)
                self.assertNotIn(key, d3)
                self.assertIn(key, d4)
                
                self.assertEqual(d[key], d2[key])
                self.assertEqual(d[key], d4[key])

if __name__ == '__main__':
    main()
