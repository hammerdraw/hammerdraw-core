from unittest import TestCase

from hammerdraw.util.joiner import join_list

class CommonTests(TestCase):

    def do_test(self, x, estimated_result_1, estimated_result_2):
        result = join_list(x)
        print('"{:}"'.format(result))
        self.assertEqual(result, estimated_result_1)
        result = join_list(x, ';', ' finally ')
        print('"{:}"'.format(result))
        self.assertEqual(result, estimated_result_2)
        
    def test_empty_lists(self):
        data = \
        [
            None,
            list(),
            set(),
            dict(),
            '',
        ]
        for d in data:
            with self.subTest(d):
                self.do_test(d, '', '')
    
    def test_common_case(self):
        data = \
        [
            ([ "first element" ], "first element", "first element"),
            ([ "first element", "second element" ], "first element, second element", "first element finally second element"),
            ([ "first element",  "second element", "third element" ], "first element, second element, third element", "first element;second element finally third element"),
            ([ "first element", "second element", "third element", "fourth element" ], "first element, second element, third element, fourth element", "first element;second element;third element finally fourth element"),
            ([ "one", 2, 2.5, "three" ], "one, 2, 2.5, three", "one;2;2.5 finally three"),
        ]
        for params in data:
            with self.subTest(params):
                self.do_test(*params)

    def test_formatter(self):
        data = \
        [
            ([ "one", 2, 2.5, "three" ], "!{0}!", "!one!, !2!, !2.5!, !three!", "!one!;!2!;!2.5! finally !three!"),
            ([ 16, 2, -42, 621 ], "{:x}", "10, 2, -2a, 26d", "10;2;-2a finally 26d"),
            ([ 16, 2, -42, 621 ], hex, "0x10, 0x2, -0x2a, 0x26d", "0x10;0x2;-0x2a finally 0x26d"),
            ([ 16, 2, -42, 621 ], lambda x: oct(abs(x)), "0o20, 0o2, 0o52, 0o1155", "0o20;0o2;0o52 finally 0o1155"),
            ([ "first element", "second element", "third element", "fourth element" ], lambda x: "**{0}**".format(x.capitalize()), "**First element**, **Second element**, **Third element**, **Fourth element**", "**First element**;**Second element**;**Third element** finally **Fourth element**"),
        ]
        for params in data:
            with self.subTest(params):
                x, formatter, estimated_result_1, estimated_result_2 = params
                result = join_list(x, formatter=formatter)
                print('"{:}"'.format(result))
                self.assertEqual(result, estimated_result_1)
                result = join_list(x, ';', ' finally ', formatter=formatter)
                print(f'{result!r}')
                self.assertEqual(result, estimated_result_2)

class CombatTestingCase(TestCase):
    def test_card_categories_living_fortress(self):
        categories = [ "armour" ]
        name = "Living Fortress"
        expected = "Living Fortress is **Armour**."
        
        categories_str = join_list(categories, last_separator=' and ', formatter=lambda s: "**{0}**".format(s.capitalize()))
        result = f"{name} is {categories_str}."
        print(f'{result!r}')
        self.assertEqual(result, expected)
    
    def test_card_categories_ring_of_celerity(self):
        categories= [ "move", "dices", "reusable" ]
        name = "Ring of Celerity"
        expected = "Ring of Celerity is **Move**, **Dices** and **Reusable**."
        
        categories_str = join_list(categories, last_separator=' and ', formatter=lambda s: "**{0}**".format(s.capitalize()))
        result = f"{name} is {categories_str}."
        print(f'{result!r}')
        self.assertEqual(result, expected)

if (__name__ == '__main__'):
    from unittest import main
    main()
